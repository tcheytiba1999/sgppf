<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\AnneeRepository;
use App\Traits\EntityUseNoCodeTrait;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=AnneeRepository::class)
 * @UniqueEntity("libelle")
 */
class Annee
{
    use EntityUseNoCodeTrait;
    const ETAT = [
        'INACTIF' => 'INACTIF',
        'ACTIF' => 'ACTIF'
    ];
    const CLOTURE = 'CLOTURE';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=4, max=9)
     */
    private $libelle;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $etat = Annee::ETAT['INACTIF'];

    /**
     * @ORM\OneToMany(targetEntity=Obligation::class, mappedBy="annee")
     */
    private $obligations;

    /**
     * @ORM\OneToMany(targetEntity=Amortissement::class, mappedBy="annee")
     */
    private $amortissements;

    public function __construct()
    {
        $this->obligations = new ArrayCollection();
        $this->amortissements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * @return Collection|Obligation[]
     */
    public function getObligations(): Collection
    {
        return $this->obligations;
    }

    public function addObligation(Obligation $obligation): self
    {
        if (!$this->obligations->contains($obligation)) {
            $this->obligations[] = $obligation;
            $obligation->setAnnee($this);
        }

        return $this;
    }

    public function removeObligation(Obligation $obligation): self
    {
        if ($this->obligations->removeElement($obligation)) {
            // set the owning side to null (unless already changed)
            if ($obligation->getAnnee() === $this) {
                $obligation->setAnnee(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Amortissement[]
     */
    public function getAmortissements(): Collection
    {
        return $this->amortissements;
    }

    public function addAmortissement(Amortissement $amortissement): self
    {
        if (!$this->amortissements->contains($amortissement)) {
            $this->amortissements[] = $amortissement;
            $amortissement->setAnnee($this);
        }

        return $this;
    }

    public function removeAmortissement(Amortissement $amortissement): self
    {
        if ($this->amortissements->removeElement($amortissement)) {
            // set the owning side to null (unless already changed)
            if ($amortissement->getAnnee() === $this) {
                $amortissement->setAnnee(null);
            }
        }

        return $this;
    }
}
