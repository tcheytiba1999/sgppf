<?php

namespace App\Entity;

use App\Traits\EntityUseTrait;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\RoleRepository;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=RoleRepository::class)
 */
class Role
{
    use EntityUseTrait;
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\SecureRoute", inversedBy="roles")
     */
    private $seruredRoutes;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="userRoles")
     */
    private $users;

    public function __construct()
    {
        $this->seruredRoutes = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function  __toString(){
        return $this ->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addUserRole($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeUserRole($this);
        }

        return $this;
    }

    /**
     * @return Collection|SecureRoute[]
     */
    public function getSeruredRoutes(): Collection
    {
        return $this->seruredRoutes;
    }

    public function addSeruredRoute(SecureRoute $seruredRoute): self
    {
        if (!$this->seruredRoutes->contains($seruredRoute)) {
            $this->seruredRoutes[] = $seruredRoute;
        }

        return $this;
    }

    public function removeSeruredRoute(SecureRoute $seruredRoute): self
    {
        if ($this->seruredRoutes->contains($seruredRoute)) {
            $this->seruredRoutes->removeElement($seruredRoute);
        }

        return $this;
    }
}
