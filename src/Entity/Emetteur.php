<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Traits\EntityUseNoCodeTrait;
use App\Repository\EmetteurRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=EmetteurRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Emetteur
{
    use EntityUseNoCodeTrait;
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $entite;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $contact;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity=Obligation::class, mappedBy="emetteur", orphanRemoval=true)
     */
    private $obligations;

    /**
     * @ORM\OneToMany(targetEntity=Amortissement::class, mappedBy="emetteur", orphanRemoval=true)
     */
    private $amortissement;

    public function __construct()
    {
        $this->obligations = new ArrayCollection();
        $this->amortissement = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getEntite(): ?string
    {
        return $this->entite;
    }

    public function setEntite(string $entite): self
    {
        $this->entite = $entite;

        return $this;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function setContact(?string $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|Obligation[]
     */
    public function getObligations(): Collection
    {
        return $this->obligations;
    }

    public function addObligation(Obligation $obligation): self
    {
        if (!$this->obligations->contains($obligation)) {
            $this->obligations[] = $obligation;
            $obligation->setEmetteur($this);
        }

        return $this;
    }

    public function removeObligation(Obligation $obligation): self
    {
        if ($this->obligations->removeElement($obligation)) {
            // set the owning side to null (unless already changed)
            if ($obligation->getEmetteur() === $this) {
                $obligation->setEmetteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Amortissement[]
     */
    public function getAmortissements(): Collection
    {
        return $this->amortissements;
    }

    public function addAmortissement(Amortissement $amortissement): self
    {
        if (!$this->amortissements->contains($amortissement)) {
            $this->amortissements[] = $amortissement;
            $amortissement->setEmetteur($this);
        }

        return $this;
    }

    public function removeAmortissement(Amortissement $amortissement): self
    {
        if ($this->amortissements->removeElement($amortissement)) {
            // set the owning side to null (unless already changed)
            if ($amortissement->getEmetteur() === $this) {
                $amortissement->setEmetteur(null);
            }
        }

        return $this;
    }
}
