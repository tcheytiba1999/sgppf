<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Filtre
{    
    /**
     * @var datetime|null
     */
    private $dateD;

    /**
     * @var datetime|null
     * @Assert\Range(minPropertyPath="dateD")
     */
    private $dateF;

    /**
     * @var date|null
     */
    private $date;

    /**
     * @var string|null
     */
    private $libelle;

    /**
     * @var int|null
     * @Assert\Range(minPropertyPath="min")
     */
    private $max;

    /**
     * @var int|null
     * @Assert\Range(min=0)
     */
    private $min;

    /**
     * @var string|null
     */
    private $order;

    /**
     * @var int|null
     * @Assert\Range(min=0)
     */
    private $limit;
     
    public function getDateD()
    {
        return $this->dateD;
    }
     
    public function setDateD($dateD): self
    {
        $this->dateD = $dateD;

        return $this;
    }
     
    public function getDateF()
    {
        return $this->dateF;
    }
     
    public function setDateF($dateF):self
    {
        $this->dateF = $dateF;

        return $this;
    }
     
    public function getDate()
    {
        return $this->date;
    }
     
    public function setDate($date):self
    {
        $this->date = $date;

        return $this;
    }
     
    public function getMax(): ?int
    {
        return $this->max;
    }
     
    public function setMax($max):self
    {
        $this->max = $max;

        return $this;
    }
     
    public function getMin(): ?int
    {
        return $this->min;
    }
    
    public function setMin($min):self
    {
        $this->min = $min;

        return $this;
    }
     
    public function getOrder(): ?string
    {
        return $this->order;
    }
    
    public function setOrder($order):self
    {
        $this->order = $order;

        return $this;
    }
     
    public function getLimit(): ?int
    {
        return $this->limit;
    }
    
    public function setLimit($limit):self
    {
        $this->limit = $limit;

        return $this;
    }
    
    public function getLibelle(): ?string
    {
        return $this->libelle;
    }
     
    public function setLibelle($libelle):self
    {
        $this->libelle = $libelle;

        return $this;
    }
}