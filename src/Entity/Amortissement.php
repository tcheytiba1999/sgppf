<?php

namespace App\Entity;

use App\Repository\AmortissementRepository;
use App\Traits\EntityUseTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AmortissementRepository::class)
 */
class Amortissement
{
    use EntityUseTrait;
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Emetteur::class, inversedBy="amortissements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $emetteur;

    /**
     * @ORM\OneToMany(targetEntity=OptionAmortissement::class, mappedBy="amortissement")
     */
    private $options;

    /**
     * @ORM\OneToOne(targetEntity=Obligation::class, mappedBy="amortissement", cascade={"persist", "remove"})
     */
    private $obligation;

    /**
     * @ORM\ManyToOne(targetEntity=Annee::class, inversedBy="amortissements")
     */
    private $annee;

    public function __construct()
    {
        $this->options = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmetteur(): ?Emetteur
    {
        return $this->emetteur;
    }

    public function setEmetteur(?Emetteur $emetteur): self
    {
        $this->emetteur = $emetteur;

        return $this;
    }

    public function getObligation(): ?Obligation
    {
        return $this->obligation;
    }

    public function setObligation(?Obligation $obligation): self
    {
        // unset the owning side of the relation if necessary
        if ($obligation === null && $this->obligation !== null) {
            $this->obligation->setAmortissement(null);
        }

        // set the owning side of the relation if necessary
        if ($obligation !== null && $obligation->getAmortissement() !== $this) {
            $obligation->setAmortissement($this);
        }

        $this->obligation = $obligation;

        return $this;
    }

    public function getAnnee(): ?Annee
    {
        return $this->annee;
    }

    public function setAnnee(?Annee $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * @return Collection|OptionAmortissement[]
     */
    public function getOptions(): Collection
    {
        return $this->options;
    }

    public function addOption(OptionAmortissement $option): self
    {
        if (!$this->options->contains($option)) {
            $this->options[] = $option;
            $option->setAmortissement($this);
        }

        return $this;
    }

    public function removeOption(OptionAmortissement $option): self
    {
        if ($this->options->removeElement($option)) {
            // set the owning side to null (unless already changed)
            if ($option->getAmortissement() === $this) {
                $option->setAmortissement(null);
            }
        }

        return $this;
    }
}
