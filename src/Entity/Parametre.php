<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Traits\EntityUseNoCodeTrait;
use App\Repository\ParametreRepository;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass=ParametreRepository::class)
 * @Vich\Uploadable()
 */
class Parametre
{
    use EntityUseNoCodeTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $societe;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $siege;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $directeur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(min=14, max=30)
     */
    private $contact;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $monnaie;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $theme;

    /**
     * @ORM\Column(type="text", length=255, nullable=true)
     */
    private $piedsPage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $hautPage;

    /**
     * @Vich\UploadableField(mapping="upload_docs", fileNameProperty="logoName")
     * @var File|null
     * @Assert\Image(
     *     maxSize = "200k",
     *     maxSizeMessage = "Veuillez soumettre des fichiers qui n'excédent pas 200K.",
     *     mimeTypes={"image/png", "image/jpeg", "image/jpg"},
     *     mimeTypesMessage = "Veuillez soumettre des fichiers de type (png , jpeg, jpg).")
     */
    private $logoFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $logoName;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSociete(): ?string
    {
        return $this->societe;
    }

    public function setSociete(?string $societe): self
    {
        $this->societe = $societe;

        return $this;
    }

    public function getSiege(): ?string
    {
        return $this->siege;
    }

    public function setSiege(?string $siege): self
    {
        $this->siege = $siege;

        return $this;
    }

    public function getDirecteur(): ?string
    {
        return $this->directeur;
    }

    public function setDirecteur(?string $directeur): self
    {
        $this->directeur = $directeur;

        return $this;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function setContact(?string $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getMonnaie(): ?string
    {
        return $this->monnaie;
    }

    public function setMonnaie(?string $monnaie): self
    {
        $this->monnaie = $monnaie;

        return $this;
    }

    public function getTheme(): ?string
    {
        return $this->theme;
    }

    public function setTheme(?string $theme): self
    {
        $this->theme = $theme;

        return $this;
    }

    public function getPiedsPage(): ?string
    {
        return $this->piedsPage;
    }

    public function setPiedsPage(?string $piedsPage): self
    {
        $this->piedsPage = $piedsPage;

        return $this;
    }

    public function getHautPage(): ?string
    {
        return $this->hautPage;
    }

    public function setHautPage(?string $hautPage): self
    {
        $this->hautPage = $hautPage;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return File|null
     */ 
    public function getLogoFile()
    {
        return $this->logoFile;
    }

    /**
     * @param File|null $logoFile
     * @return self
     */ 
    public function setLogoFile(?File $logoFile): Parametre
    {
        $this->logoFile = $logoFile;
        if ($this->logoFile instanceof UploadedFile) {
            $this->updatedAt = new \Datetime('now');
        }        
        return $this;
    }

    /**
     * @return  string
     */ 
    public function getLogoName()
    {
        return $this->logoName;
    }

    /**
     * @param string  $logoName
     * @return self
     */ 
    public function setLogoName(?string $logoName)
    {
        $this->logoName = $logoName;

        return $this;
    }
}
