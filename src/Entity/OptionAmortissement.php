<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Traits\EntityUseNoCodeTrait;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use App\Repository\OptionAmortissementRepository;

/**
 * @ORM\Entity(repositoryClass=OptionAmortissementRepository::class)
 */
class OptionAmortissement
{
    use EntityUseNoCodeTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $periode;

    /**
     * @ORM\Column(type="float")
     */
    private $capitalD;

    /**
     * @ORM\Column(type="float")
     */
    private $capitalF;

    /**
     * @ORM\Column(type="float")
     */
    private $interet;

    /**
     * @ORM\Column(type="float")
     */
    private $amorti;

    /**
     * @ORM\Column(type="float")
     */
    private $annuite;

    /**
     * @ORM\ManyToOne(targetEntity=Amortissement::class, inversedBy="options")
     */
    private $amortissement;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPeriode(): ?string
    {
        return $this->periode;
    }

    public function setPeriode(string $periode): self
    {
        $this->periode = $periode;

        return $this;
    }

    public function getCapitalD(): ?float
    {
        return $this->capitalD;
    }

    public function setCapitalD(float $capitalD): self
    {
        $this->capitalD = $capitalD;

        return $this;
    }

    public function getCapitalF(): ?float
    {
        return $this->capitalF;
    }

    public function setCapitalF(float $capitalF): self
    {
        $this->capitalF = $capitalF;

        return $this;
    }

    public function getInteret(): ?float
    {
        return $this->interet;
    }

    public function setInteret(float $interet): self
    {
        $this->interet = $interet;

        return $this;
    }

    public function getAmorti(): ?float
    {
        return $this->amorti;
    }

    public function setAmorti(float $amorti): self
    {
        $this->amorti = $amorti;

        return $this;
    }

    public function getAnnuite(): ?float
    {
        return $this->annuite;
    }

    public function setAnnuite(float $annuite): self
    {
        $this->annuite = $annuite;

        return $this;
    }

    public function getAmortissement(): ?Amortissement
    {
        return $this->amortissement;
    }

    public function setAmortissement(?Amortissement $amortissement): self
    {
        $this->amortissement = $amortissement;

        return $this;
    }
}
