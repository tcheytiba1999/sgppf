<?php

namespace App\Entity;

use App\Traits\EntityUseTrait;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\ObligationRepository;

/**
 * @ORM\Entity(repositoryClass=ObligationRepository::class)
 */
class Obligation
{
    use EntityUseTrait;
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     */
    private $duree;

    /**
     * @ORM\Column(type="float")
     */
    private $interet;

    /**
     * @ORM\Column(type="integer")
     */
    private $nominal;

    /**
     * @ORM\Column(type="float")
     */
    private $montant;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="date")
     */
    private $grace;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbr;

    /**
     * @ORM\Column(type="integer")
     */
    private $souscrit;

    /**
     * @ORM\OneToOne(targetEntity=Amortissement::class, inversedBy="obligation", cascade={"persist", "remove"})
     */
    private $amortissement;

    /**
     * @ORM\ManyToOne(targetEntity=Annee::class, inversedBy="obligations")
     */
    private $annee;

    /**
     * @ORM\ManyToOne(targetEntity=Sgi::class, inversedBy="obligations")
     */
    private $sgi;

    /**
     * @ORM\ManyToOne(targetEntity=Emetteur::class, inversedBy="obligations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $emetteur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getDuree(): ?int
    {
        return $this->duree;
    }

    public function setDuree(int $duree): self
    {
        $this->duree = $duree;

        return $this;
    }

    public function getInteret(): ?float
    {
        return $this->interet;
    }

    public function setInteret(float $interet): self
    {
        $this->interet = $interet;

        return $this;
    }

    public function getNominal(): ?int
    {
        return $this->nominal;
    }

    public function setNominal(int $nominal): self
    {
        $this->nominal = $nominal;

        return $this;
    }

    public function getMontant(): ?float
    {
        return $this->montant;
    }

    public function setMontant(float $montant): self
    {
        $this->montant = $montant;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getGrace(): ?\DateTimeInterface
    {
        return $this->grace;
    }

    public function setGrace(\DateTimeInterface $grace): self
    {
        $this->grace = $grace;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getNbr(): ?int
    {
        return $this->nbr;
    }

    public function setNbr(int $nbr): self
    {
        $this->nbr = $nbr;

        return $this;
    }

    public function getSouscrit(): ?int
    {
        return $this->souscrit;
    }

    public function setSouscrit(int $souscrit): self
    {
        $this->souscrit = $souscrit;

        return $this;
    }

    public function getAmortissement(): ?Amortissement
    {
        return $this->amortissement;
    }

    public function setAmortissement(?Amortissement $amortissement): self
    {
        $this->amortissement = $amortissement;

        return $this;
    }

    public function getAnnee(): ?Annee
    {
        return $this->annee;
    }

    public function setAnnee(?Annee $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

    public function getSgi(): ?Sgi
    {
        return $this->sgi;
    }

    public function setSgi(?Sgi $sgi): self
    {
        $this->sgi = $sgi;

        return $this;
    }

    public function getEmetteur(): ?Emetteur
    {
        return $this->emetteur;
    }

    public function setEmetteur(?Emetteur $emetteur): self
    {
        $this->emetteur = $emetteur;

        return $this;
    }

    public function getTypeL(): ?string
    {
        $libelle = "";
        switch ($this->type) {
            case 'AMORTISMENT':
                $libelle = $this->type . " CONSTANT";
                break;
            case 'CONSTANTE':
                $libelle = $this->type . " CONSTANTE";
                break;
            case 'FINE':
                $libelle = "IN " . $this->type;
                break;
        }
        return $libelle;
    }
}
