<?php
namespace App\Command;

use App\Entity\SecureRoute;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Routing\RouterInterface;

class RoleCommand extends Command
{
    protected static $defaultName = 'insert:route';
    private $route;
    private $em;

    public function __construct(RouterInterface $route, EntityManagerInterface $em)
    {
        $this->route = $route;
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Update secured routes')
            ->setHelp('This command allow you to update all secured routes ...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $allRoutes = $this->route->getRouteCollection()->all();
        foreach ($allRoutes as $name => $route) {
            $options = $route->getOptions();
            if(preg_match('#^/privated#', $route->getPath()) or $route->getPath() === "/") {
               $route = $route->getPath();
                (isset($options["description"])) ? $description = $options["description"] : $description = "" ;
                $roleTest = $this->em->getRepository(SecureRoute::class)->findOneBy(array('name'=>  $name) );
                if(!$roleTest) {
                    $securedRoute = new SecureRoute();
                    $securedRoute->setName($name);
                    $securedRoute->setPath($route);
                    $securedRoute->setDescription($description);
                    $this->em->persist($securedRoute);
                    $this->em->flush();
                }
            }
        }

        $io->success('Secured routes are update with success ...');
        return 0;
    }
}