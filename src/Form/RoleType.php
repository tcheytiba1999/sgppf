<?php

namespace App\Form;

use App\Entity\Role;
use App\Entity\SecureRoute;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class RoleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'label' => "Libellé *"
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'label' => "Description"
            ])
            ->add('seruredRoutes', EntityType::class, [
                'class' => SecureRoute::class,
                'label' => "Routes *",
                'choice_label' => 'description',
                'multiple' => true,
                'required' => true,
                'attr' => [
                    'class' => 'select',
                    'data-live-search' => true,
                    'data-actions-box' => true
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Role::class,
        ]);
    }
}
