<?php

namespace App\Form;

use App\Entity\Filtre;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FiltreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateD', DateType::class, [
                'required' => false,
                'label' => 'Date de début',
                'widget' => 'single_text',
                'empty_data'  => '',
                'attr' => [
                    'placeholder' => 'Date de début'
                ],
            ])
            ->add('dateF', DateType::class, [
                'required' => false,
                'label' => 'Date de fin',
                'widget' => 'single_text',
                'empty_data'  => '',
                'attr' => [
                    'placeholder' => 'Date de fin'
                ],
            ])
            ->add('date', DateType::class, [
                'required' => false,
                'label' => 'Date de création',
                'widget' => 'single_text',
                'empty_data'  => '',
                'attr' => [
                    'placeholder' => 'Date de création'
                ],
            ])
            ->add('min', IntegerType::class, [
                'required' => false,
                'label' => 'Minimum',
                'attr' => [
                    'min' => 0,
                    'placeholder' => 'Minimum'
                ],
            ])
            ->add('max', IntegerType::class, [
                'required' => false,
                'label' => 'Maximum',
                'attr' => [
                    'min' => 0,
                    'placeholder' => 'Maximum'
                ],
            ])
            ->add('order', ChoiceType::class, [
                'required' => true,
                'label' => 'Ordre',
                'choices' => [
                    'Croissant' => 'ASC',
                    'Décroissant' => 'DESC'
                ]
            ])
            ->add('limit', ChoiceType::class, [
                'required' => true,
                'label' => 'Limite',
                'choices' => [
                    'Tout' => 0,
                    '1' => 1,
                    '5' => 5,
                    '10' => 10,
                    '20' => 20,
                    '50' => 50,
                    '100' => 100,
                    '200' => 200
                ]
            ])
            ->add('libelle', TextType::class, [
                'required' => false,
                'label' => 'Nom et prénoms / Libellé',
                'attr' => [
                    'placeholder' => 'Nom et prénoms / Libellé',
                    'class' => 'libelleV'
                ]
            ])
        ;
        if ($options['dateDReq'] == false) {
            $builder->remove("dateD");
        }
        if ($options['dateFReq'] == false) {
            $builder->remove("dateF");
        }
        if ($options['dateReq'] == false) {
            $builder->remove("date");
        }
        if ($options['minReq'] == false) {
            $builder->remove("min");
        }
        if ($options['maxReq'] == false) {
            $builder->remove("max");
        }
        if ($options['orderReq'] == false) {
            $builder->remove("order");
        }
        if ($options['limitReq'] == false) {
            $builder->remove("limit");
        }
        if ($options['libelleReq'] == false) {
            $builder->remove("libelle");
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Filtre::class,
            'csrf_protection' => false,
            'dateDReq' => false,
            'dateFReq' => false,
            'dateReq' => false,
            'minReq' => false,
            'maxReq' => false,
            'libelleReq' => false,
            'orderReq' => false,
            'limitReq' => false,
        ]);
    }    
}
