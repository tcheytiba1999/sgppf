<?php

namespace App\Form;

use App\Entity\Sgi;
use App\Entity\Emetteur;
use App\Entity\Obligation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;

class ObligationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sgi', EntityType::class, [
                'required' => true,
                'label' => false,
                'class' => Sgi::class,
                'choice_label' => 'nom',
                'multiple' => false,
                'attr' => [
                    'placeholder' => "SGI"
                ]
            ])
            ->add('libelle', TextType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => "Nom"
                ]
            ])
            ->add('emetteur', EntityType::class, [
                'required' => true,
                'label' => false,
                'class' => Emetteur::class,
                'choice_label' => 'nom',
                'multiple' => false,
                'attr' => [
                    'placeholder' => "Emetteur"
                ]
            ])
            ->add('duree', ChoiceType::class, [
                'label' => false,
                'choices' => [
                    1 => 1,
                    2 => 2,
                    3 => 3,
                    4 => 4,
                    5 => 5,
                    6 => 6,
                    7 => 7,
                    8 => 8,
                    9 => 9,
                    10 => 10,
                    11 => 11,
                    12 => 12,
                    13 => 13,
                    14 => 14,
                    15 => 15,
                    16 => 16,
                    17 => 17,
                    18 => 18,
                    19 => 19,
                    20 => 20
                ]
            ])
            ->add('interet', PercentType::class, [
                'label' => false,
                'attr' => [
                    'min' => 0,
                    'placeholder' => "Taux d'intérêt"
                ]
            ])
            ->add('nominal', IntegerType::class, [
                'label' => false,
                'attr' => [
                    'min' => 0,
                    'class' => 'nominal',
                    'placeholder' => "Montant nominal"
                ]
            ])
            ->add('montant', IntegerType::class, [
                'label' => false,
                'attr' => [
                    'min' => 0,
                    'class' => 'montant text-danger font-weight-bold',
                    'placeholder' => "Montant souscrit",
                    'readonly' => true
                ]
            ])
            ->add('type', ChoiceType::class, [
                'label' => false,
                'choices' => [
                    'AMORTISSEMENT CONSTANT' => 'AMORTISMENT',
                    'IN FINE' => 'FINE',
                    'ANNUITES CONSTANTES' => 'CONSTANTE'
                ]
            ])
            ->add('grace', DateType::class, [
                'label' => false,
                'widget' => 'single_text',
                'empty_data'  => ''
            ])
            ->add('date', DateType::class, [
                'label' => false,
                'widget' => 'single_text',
                'empty_data'  => ''
            ])
            ->add('nbr', IntegerType::class, [
                'label' => false,
                'attr' => [
                    'min' => 0,
                    'placeholder' => "Nombre de rembousement"
                ]
            ])
            ->add('souscrit', IntegerType::class, [
                'label' => false,
                'attr' => [
                    'min' => 0,
                    'class' => 'souscrit',
                    'placeholder' => "Nbr d'obligation souscrit"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Obligation::class,
        ]);
    }
}
