<?php

namespace App\Form;

use App\Entity\Groupe;
use App\Entity\User;
use App\Entity\Role;
use App\Entity\Service;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('username', TextType::class, [
                'required' => true,
                'label' => "Nom d'utilisateur *"
            ])
            ->add('nom', TextType::class, [
                'required' => true,
                'label' => "Nom *"
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => "E-mail *"
            ])
            ->add('prenom', TextType::class, [
                'required' => true,
                'label' => "Prénom *"
            ])
            ->add('dateN', DateType::class, [
                'required' => false,
                'label' => 'Date de naissance',
                'widget' => 'single_text',
                'empty_data'  => ''
            ])
            ->add('fonction')
            ->add('password', PasswordType::class, [
                'required' => true,
                'label' => "Mot de passe *"
            ])
            ->add('contact', TextType::class, [
                'required' => true,
                'label' => "Contact *"
            ])
            ->add('imageFile', FileType::class, [
                'required' => false,
                'label' => 'Photo',
                'attr' => [
                    'class' => '',
                    'is' => 'drop-files',
                ]
            ])
            ->add('userRoles', EntityType::class, [
                'class' => Role::class,
                'label' => "Droîts d'accès *",
                'choice_label' => 'name',
                'multiple' => true,
                'required' => true,
                'attr' => [
                    'class' => 'select',
                    'data-live-search' => true,
                    'data-actions-box' => true
                ]
            ])
            ->add('service', EntityType::class, [
                'required' => true,
                'label' => 'Service *',
                'choice_label' => 'libelle',
                'class' => Service::class
            ])
            ->add('groupe', EntityType::class, [
                'required' => true,
                'label' => "Groupe d'utilisateur *",
                'choice_label' => 'description',
                'class' => Groupe::class,
                'multiple' => true,
                'attr' => [
                    'class' => 'select'
                ]
            ])
        ;

        if ($options['passwordReq'] == false) {
            $builder->remove("password");
        }

        if ($options['groupeReq'] == false) {
            $builder->remove("groupe");
        }

        if ($options['userRolesReq'] == false) {
            $builder->remove("userRoles");
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'translation_domain' => 'forms',
            'passwordReq' => true,
            'groupeReq' => true,
            'userRolesReq' => true
        ]);
    }
}
