<?php

namespace App\Form;

use App\Entity\Parametre;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParametreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('societe', TextType::class, [
            'required' => true,
            'label' => 'Raison sociale *',
            'attr' => [
                'placeholder' => 'Raison sociale'
            ]
        ])
        ->add('hautPage', TextareaType::class, [
            'required' => false,
            'label' => 'Haut de page',
            'attr' => [
                'placeholder' => 'Haut de page',
                'rows' => '5'
            ]
        ])
        ->add('piedsPage', TextareaType::class, [
            'required' => false,
            'label' => 'Pieds de page',
            'attr' => [
                'placeholder' => 'Pieds de page',
                'rows' => '5'
            ]
        ])
        ->add('siege', TextType::class, [
            'required' => true,
            'label' => 'Siège sociale *',
            'attr' => [
                'placeholder' => 'Siège sociale'
            ]
        ])
        ->add('directeur', TextType::class, [
            'required' => false,
            'label' => 'Nom et Prénoms du DG *',
            'attr' => [
                'placeholder' => 'Nom et Prénoms du DG'
            ]
        ])
        ->add('contact', IntegerType::class, [
            'required' => true,
            'label' => 'Contact *',
            'attr' => [
                'placeholder' => 'Contact',
                'min' => '0'
            ]
        ])
        ->add('email', EmailType::class, [
            'required' => true,
            'label' => 'E-mail *',
            'attr' => [
                'placeholder' => 'E-mail'
            ]
            
        ])
        ->add('adresse', TextType::class, [
            'required' => false,
            'label' => 'Adresse postal',
            'attr' => [
                'placeholder' => 'Adresse postal'
            ]
        ])
        ->add('monnaie', ChoiceType::class, [
            'required' => true,
            'label' => 'Symbole monnetaire',
            'choices'  => [
                'Zone UEMOA (XOF)' => 'XOF',
                'Euro (€)' => '€',
                'Dollar ($)' => '$',
                'Livre sterling (£)' => '£'
            ],
        ])
        ->add('theme', ChoiceType::class, [
            'required' => true,
            'label' => 'Thème',
            'choices'  => [
                'Défaut' => 'primary',
                'Bleu light' => 'info',
                'Noir' => 'dark',
                'Attirant' => 'warning',
                'Vert' => 'success',
                'Attractif' => 'danger',
            ],
        ])
        ->add('logoFile', FileType::class, [
            'required' => false
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Parametre::class,
        ]);
    }
}
