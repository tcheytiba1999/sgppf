<?php

namespace App\Form;

use App\Entity\Annee;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class AnneeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle', TextType::class, [
                'required' => true,
                'label' => false,
                'attr' => [
                    'placeholder' => '0000-0000'
                ],
            ])
            ->add('description')
            // ->add('etat', ChoiceType::class, [
            //     'required' => true,
            //     'choices' => [
            //         'INACTIF' => 'INACTIF',
            //         'ACTIF' => 'ACTIF'
            //     ]
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Annee::class,
        ]);
    }
}
