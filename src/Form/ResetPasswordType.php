<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class ResetPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'Veuillez saisir votre E-mail *'
            ])
            ->add('oldPassword', PasswordType::class, [
                'mapped' => true,
                'label' => 'Ancien mot de passe *'
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les deux mots de passe doivent être identiques',
                'options' => [
                    'attr' => [
                        'class' => 'password-field'
                    ]
                ],
                'first_options' => [
                    'label' => 'Nouveau mot de passe *'
                ],
                'second_options' => [
                    'label' => 'Repétez le mot de passe *'
                ],
                'required' => true,
                'label' => 'Nouveau mot de passe'
            ])
        ;
        
        if ($options['oldPasswordReq'] == false) {
            $builder->remove("oldPassword");
        }
        if ($options['passwordReq'] == false) {
            $builder->remove("password");
        }
        if ($options['emailReq'] == false) {
            $builder->remove("email");
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'oldPasswordReq' => true,
            'passwordReq' => true,
            'emailReq' => false
        ]);
    }
}
