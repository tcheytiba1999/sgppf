<?php

namespace App\Form;

use App\Entity\Sgi;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SgiType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'required' => true,
                'attr' => [
                    'placeholder' => 'Nom et prénoms'
                ]
            ])
            ->add('contact', TextType::class, [
                'required' => true,
                'attr' => [
                    'placeholder' => '+225 00 00 00 00 00'
                ]
            ])
            ->add('entreprise', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Travail pour le compte ?'
                ]
            ])
            ->add('adresse', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Adresse'
                ]
            ])
            ->add('domicile', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Domicilié à'
                ]
            ])
            ->add('portefeuil', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Portefeuil en charge'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sgi::class,
        ]);
    }
}
