<?php

namespace App\Services;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RolesService extends AbstractController
{
    public function droits() {
        $routes = [];
        foreach ($this->getUser()->getUserRoles() as $roles) {
            foreach ($roles->getSeruredRoutes() as $route) {
                $routes[] = $route->getName();
            }
        }
        
        return $routes;
    }
}
