<?php

namespace App\Services;

use App\Repository\AnneeRepository;
use App\Repository\ParametreRepository;

class ParametreService
{
    private $parametreRepository;
    private $anneeRepository;
    public function __construct(ParametreRepository $parametreRepository, AnneeRepository $anneeRepository)
    {
        $this->parametreRepository = $parametreRepository;
        $this->anneeRepository = $anneeRepository;
    }

    public function general() {
        $param = $this->parametreRepository->find(1);
        return $param;
    }

    public function theme() {
        $param = $this->parametreRepository->find(1);
        $theme = (!empty($param) && $param) ? $param->getTheme() : 'info';
        return $theme;
    }

    public function sym() {
        $param = $this->parametreRepository->find(1);
        $monnaie = (!empty($param) && $param) ? $param->getMonnaie() : 'XOF';
        return $monnaie;
    }

    public function annee() {
        $annee = $this->anneeRepository->findOneByEtat('ACTIF');
        return (!empty($annee) && $annee) ? $annee : null;
    }
}
