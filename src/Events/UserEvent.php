<?php

namespace App\Events;

use App\Entity\SecureRoute;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class UserEvent
{
    private $security;
 
    public function __construct(
        Security $security
    ) {
        $this->security = $security;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof SecureRoute) {
            return;
        }
        $user = $this->security->getUser();
        if ($user instanceof UserInterface && method_exists($entity, 'setUpdateBy')) {
            $entity->setCreateBy($user);
        }
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof SecureRoute) {
            return;
        }
        $user = $this->security->getUser();
        if ($user instanceof UserInterface && method_exists($entity, 'setUpdateBy')) {
            $entity->setUpdateBy($user);
        }
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof SecureRoute) {
            return;
        }
        $user = $this->security->getUser();
        if ($user instanceof UserInterface && method_exists($entity, 'setRemoveBy')) {
            $entity->setRemoveBy($this->user);
        }
    }
}
