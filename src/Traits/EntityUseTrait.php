<?php

namespace App\Traits;

use App\Entity\User;
use Gedmo\Mapping\Annotation as Gedmo;

trait EntityUseTrait
{
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $createBy;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $updateBy;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $removeBy;
    
    public function getCode()
    {
        return $this->code;
    }
    
    public function setCode(?string $code):self
    {
        $this->code = $code;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
    
    public function getCreateBy(): ?User
    {
        return $this->createBy;
    }
    
    public function setCreateBy(?User $createBy): self
    {
        $this->createBy = $createBy;

        return $this;
    }
    
    public function getUpdateBy(): ?User
    {
        return $this->updateBy;
    }
    
    public function setUpdateBy(?User $updateBy): self
    {
        $this->updateBy = $updateBy;

        return $this;
    }
    
    public function getRemoveBy(): ?User
    {
        return $this->removeBy;
    }
    
    public function setRemoveBy(?User $removeBy): self
    {
        $this->removeBy = $removeBy;

        return $this;
    }
}
