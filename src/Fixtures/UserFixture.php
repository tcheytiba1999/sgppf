<?php

namespace App\Fixtures;

use App\Entity\Groupe;
use App\Entity\Role;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends Fixture
{
    private $passwordEncoder;
    private $userRepository;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, UserRepository $userRepository)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->userRepository = $userRepository;
    }

    public function load(ObjectManager $manager)
    {
        $check = $this->userRepository->findOneBy(['first' => true]);
        if (!$check) {
            //GROUPE
            $groupe = new Groupe();
            $groupe
                ->setName('USER')
                ->setRole('ROLE_USER')
                ->setDescription('Rôle utilisateur')
            ;
            $manager->persist($groupe);
            $manager->flush();
    
            //ROLE
            $role = new Role();
            $role
                ->setName('Accès super administrateur')
                ->setDescription('Droits sur les actions du super adminsitrateur')
                ->setCreatedAt(new \DateTime('now'))
            ;
            $manager->persist($role);
            $manager->flush();
    
            //USER
            $user = new User();
            $user
                ->setUsername('admin')
                ->setPassword($this->passwordEncoder->encodePassword($user, 'admin'))
                ->setNom('SGPP')
                ->setPrenom('ADMIN')
                ->setEmail('gnahouaanicet@gmail.com')
                ->setContact('+2250748867578')
                ->setFirst(true)
                ->addGroupe($groupe)
                ->addUserRole($role)
                ->setCreatedAt(new \DateTime('now'))
            ;
    
            $manager->persist($user);
            $manager->flush();
        }
    }
}