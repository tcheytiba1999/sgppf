<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class TwigBootstrapExtension extends AbstractExtension
{
    public function getFilters() {
        return [
            new TwigFilter('label', [$this, 'labelFilter'], ['is_safe' => ['html']]),
            new TwigFilter('boolLabel', [$this, 'boolLabelFilter'], ['is_safe' => ['html']]),
            new TwigFilter('color', [$this, 'colorFilter'], ['is_safe' => ['html']]),
            new TwigFilter('bre', [$this, 'breFilter']),
            new TwigFilter('sortBy', [$this, 'sortBy'])
        ];
    }

    public function colorFilter($content, array $options = []): string {
        $defaultOptions = [
            'color' => 'primary',
            'class' => '',
            'content' => '',
            'icon' => ''
        ];
        $options = array_merge($defaultOptions, $options);

        $color = $options['color'];
        $class = $options['class'];
        $autre = $options['content'];
        $icon = $options['icon'];

        $template = '<span class="text-%s %s">%s %s<i class="%s"></i></span>';
        return sprintf($template, $color, $class, $content, $autre, $icon);
    }

    public function labelFilter($content, array $options = []): string {
        $defaultOptions = [
            'color' => 'primary',
            'rounded' => false, 
            'class' => '',
            'content' => '',
            'icon' => ''
        ];
        $options = array_merge($defaultOptions, $options);

        $color = $options['color'];
        $pill = $options['rounded'] ? " badge-pill" : "";
        $class = $options['class'];
        $autre = $options['content'];
        $icon = $options['icon'];

        $template = '<span class="badge badge-%s %s %s">%s %s <i class="%s"></i></span>';
        return sprintf($template, $color, $pill, $class, $content, $autre, $icon);
    }

    public function boolLabelFilter(bool $content, array $options = []) : string {
        $defaultOptions = [
            'trueText' => 'OUI',
            'falseText' => 'NON'
        ];
        $options = array_merge($defaultOptions, $options);
        if ($content) {
            return $this->labelFilter($options['trueText'], ['color' => 'success', 'class' => 'size']);
        } else {
            return $this->labelFilter($options['falseText'], ['color' => 'danger', 'class' => 'size']);
        }
    }

    public function breFilter($content){
        $bre = substr($content, 0, 10); //abreviation d'une chaine de carcatère
        return $bre.'...';
    }

    public function sortBy($content, $sort_by, $direction = 'ASC'){
        if (is_a($content, 'Doctrine\ORM\PersistentCollection')) {
            $content = $content->toArray();
        }
        if (!is_array($content)) {
            throw new \InvalidArgumentException('Variable passed to the sortByField filter is not an array');
        } elseif (count($content) < 1) { 
            return $content; 
        } else { 
            @usort($content, function ($a, $b) use ($sort_by, $direction) { 
                $flip = ($direction === 'DESC') ? -1 : 1; 
                if (is_array($a)) $a_sort_value = $a[$sort_by]; 
                else if (method_exists($a, 'get' . ucfirst($sort_by))) $a_sort_value = $a->{'get' . ucfirst($sort_by)}();
                else
                    $a_sort_value = $a->$sort_by;
                if (is_array($b))
                    $b_sort_value = $b[$sort_by];
                else if (method_exists($b, 'get' . ucfirst($sort_by)))
                    $b_sort_value = $b->{'get' . ucfirst($sort_by)}();
                else
                    $b_sort_value = $b->$sort_by;
                if ($a_sort_value == $b_sort_value) {
                    return 0;
                } else if ($a_sort_value > $b_sort_value) {
                    return (1 * $flip);
                } else {
                    return (-1 * $flip);
                }
            });
        }
        return $content;
    }
}
