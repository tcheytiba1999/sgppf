<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    private $userRepository;
    public function __construct(
        UserRepository $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/", name="homepage", options={"description"="Tableau de bord accueil"})
     */
    public function index(): Response
    {
        return $this->render('home.html.twig', [
            'title' => 'Accueil'
        ]);
    }

    /**
     * @Route("/private/home/obligation", name="home_obligation", 
     * options={"description"="Tableau de bord oligation"})
     */
    public function obligation(): Response
    {           
        return $this->render('obligation/home.html.twig', [
            'title' => 'Tableau de bord obligation'
        ]);
    }

    /**
     * @Route("/private/home/action", name="home_action", 
     * options={"description"="Tableau de bord action"})
     */
    public function action(): Response
    {           
        return $this->render('action/home.html.twig', [
            'title' => 'Tableau de bord action'
        ]);
    }


    /**
     * @Route("/private/home/user", name="home_user", 
     * options={"description"="Tableau de bord utilisateur"})
     */
    public function user(): Response
    {           
        $users = $this->userRepository->findBy(["first" => false], [], 10);
        return $this->render('user/home.html.twig', [
            'users' => $users,
            'title' => 'Tableau de bord utilisateur'
        ]);
    }

    /**
     * @Route("/private/home/parametre", name="home_parametre", 
     * options={"description"="Tableau de bord parametre"})
     */
    public function parametre(): Response
    {
        return $this->render('parametre/home.html.twig', [
            'title' => 'Tableau de bord parametre',
        ]);
    }
}
