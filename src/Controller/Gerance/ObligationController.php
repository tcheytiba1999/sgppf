<?php

namespace App\Controller\Gerance;

use App\Entity\Amortissement;
use App\Entity\Filtre;
use App\Entity\Obligation;
use App\Entity\OptionAmortissement;
use App\Form\FiltreType;
use App\Form\ObligationType;
use App\Repository\ObligationRepository;
use App\Services\ParametreService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/private/obligation")
 */
class ObligationController extends AbstractController
{
    private $obligationRepository;
    private $param;
    public function __construct(
        ObligationRepository $obligationRepository,
        ParametreService $param
    )
    {
        $this->obligationRepository = $obligationRepository;
        $this->param = $param;
    }

    /**
     * @Route("/situation-sorties", name="obligation_situation", methods={"GET","POST"}, 
     * options={"description"="Situation des obligations sorties"})
     */
    public function situation(Request $request):Response
    {
        $obligations =  $this->obligationRepository->findBy([], ['id' => 'DESC'], 10);
        $filtre = new Filtre();
        $form = $this->createForm(FiltreType::class, $filtre, [
            "dateDReq" => true,
            "dateFReq" => true,
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $obligations = $this->obligationRepository->findFiltre($filtre);
        }

        //dd($obligations);
        return $this->render('obligation/situation.html.twig', [
            'obligations' => $obligations,
            'form' => $form->createView(),
            'title' => 'Situation des obligations',
        ]);
        
    }

    /**
     * @Route("/situation-non-sorties", name="obligation_situation_non", methods={"GET","POST"}, 
     * options={"description"="Situation des obligations non sorties"})
     */
    public function situationnon(Request $request):Response
    {
        $obligations =  $this->obligationRepository->findBy([], ['id' => 'DESC'], 10);
        $filtre = new Filtre();
        $form = $this->createForm(FiltreType::class, $filtre, [
            "dateDReq" => true,
            "dateFReq" => true,
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $obligations = $this->obligationRepository->findFiltre($filtre);
        }

        //dd($obligations);
        return $this->render('obligation/situationnon.html.twig', [
            'obligations' => $obligations,
            'form' => $form->createView(),
            'title' => 'Situation des obligations',
        ]);
        
    }

    /**
     * @Route("/", name="obligation_index", methods={"GET","POST"}, 
     * options={"description"="Liste des obligations"})
     */
    public function index(Request $request): Response
    {
        $obligations =  $this->obligationRepository->findBy([], ['id' => 'DESC'], 10);
        $filtre = new Filtre();
        $form = $this->createForm(FiltreType::class, $filtre, [
            "dateDReq" => true,
            "dateFReq" => true,
            "libelleReq" => true,
            "orderReq" => true,
            "limitReq" => true,
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $obligations = $this->obligationRepository->findFiltre($filtre);
        }
        return $this->render('obligation/index.html.twig', [
            'obligations' => $obligations,
            'form' => $form->createView(),
            'title' => 'Liste des obligations',
        ]);
    }

    /**
     * @Route("/new", name="obligation_new", methods={"GET","POST"}, 
     * options={"description"="Ajouter une nouvelle obligation"})
     */
    public function new(Request $request): Response
    {
        $obligation = new Obligation();
        $form = $this->createForm(ObligationType::class, $obligation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $interet = $obligation->getInteret();
            $duree = $obligation->getDuree();
            $investissement = $obligation->getMontant();
            $amortissement = new Amortissement();
            $amortissement
                ->setEmetteur($obligation->getEmetteur())
                ->setAnnee($this->param->annee())
            ;
            $em->persist($amortissement);

            $obligation
                ->setAnnee($this->param->annee())
                ->setAmortissement($amortissement)
            ;
            $em->persist($obligation);
            //AMORTISSEMENT CONSTANT
            if ($obligation->getType() === 'AMORTISMENT') {
                $capitalF = 0;
                for ($i=1; $i <= $duree; $i++) {
                    $capitalD = $i === 1 ? $investissement : $capitalF;
                    $interetO = round($capitalD * $interet);
                    $amortiO = round($investissement / $duree);
                    $annuiteO = round($interetO + $amortiO);
                    $capitalF = $i === $duree ? 0 : $capitalD - $amortiO;
                    $option = new OptionAmortissement();
                    $option
                        ->setPeriode("Année ".$i)
                        ->setCapitalD($capitalD)
                        ->setInteret($interetO)
                        ->setAmorti($amortiO)
                        ->setAnnuite($annuiteO)
                        ->setCapitalF($capitalF)
                        ->setAmortissement($amortissement)
                    ;
                    $em->persist($option);
                }
            } elseif ($obligation->getType() === 'FINE') {
                for ($i=1; $i <= $duree; $i++) {
                    $capitalD =  $investissement;
                    $interetO = $investissement * $interet;
                    $amortiO = $i === $duree ? $capitalD : 0;
                    $annuiteO = round($interetO + $amortiO);
                    $capitalF = $capitalD - $amortiO;
                    $option = new OptionAmortissement();
                    $option
                        ->setPeriode("Année ".$i)
                        ->setCapitalD($capitalD)
                        ->setInteret($interetO)
                        ->setAmorti($amortiO)
                        ->setAnnuite($annuiteO)
                        ->setCapitalF($capitalF)
                        ->setAmortissement($amortissement)
                    ;
                    $em->persist($option);
                }
            } elseif ($obligation->getType() === 'CONSTANTE') {
                //ANNUITEE CONSTANT
                for ($i=1; $i <= $duree; $i++) {
                    $capitalD = $i === 1 ? $investissement : 0;
                    $interetO = round($capitalD * $interet);
                    $amortiO = round($investissement / $duree);
                    $annuiteO = round($interetO + $amortiO);
                    $capitalF = $i === $duree ? 0 : $capitalD - $amortiO;
                    $option = new OptionAmortissement();
                    $option
                        ->setPeriode("Année ".$i)
                        ->setCapitalD($capitalD)
                        ->setInteret($interetO)
                        ->setAmorti($amortiO)
                        ->setAnnuite($annuiteO)
                        ->setCapitalF($capitalF)
                        ->setAmortissement($amortissement)
                    ;
                    $em->persist($option);
                }
            }
           
            $em->flush();
            $this->addFlash('success', 'Amortissement ajouté avec succès.');
            return $this->redirectToRoute('obligation_index');
        }

        return $this->render('obligation/new.html.twig', [
            'obligation' => $obligation,
            'form' => $form->createView(),
            'title' => "Ajouter une nouvelle obligation",
        ]);
    }

    /**
     * @Route("/{id}/edit", name="obligation_edit", methods={"GET","POST"}, 
     * options={"description"="Modifier une obligation"})
     */
    public function edit(Obligation $obligation, Request $request): Response
    {
        $form = $this->createForm(ObligationType::class, $obligation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $interet = $obligation->getInteret();
            $duree = $obligation->getDuree();
            $investissement = $obligation->getMontant();
            $amortissement = $obligation->getAmortissement();
            foreach ($amortissement->getOptions() as $key => $options) {
                $em->remove($options);
            }
            
            //AMORTISSEMENT CONSTANT
            if ($obligation->getType() === 'AMORTISMENT') {
                $capitalF = 0;
                for ($i=1; $i <= $duree; $i++) {
                    $capitalD = $i === 1 ? $investissement : $capitalF;
                    $interetO = round($capitalD * $interet);
                    $amortiO = round($investissement / $duree);
                    $annuiteO = round($interetO + $amortiO);
                    $capitalF = $i === $duree ? 0 : $capitalD - $amortiO;
                    $option = new OptionAmortissement();
                    $option
                        ->setPeriode("Année ".$i)
                        ->setCapitalD($capitalD)
                        ->setInteret($interetO)
                        ->setAmorti($amortiO)
                        ->setAnnuite($annuiteO)
                        ->setCapitalF($capitalF)
                        ->setAmortissement($amortissement)
                    ;
                    $em->persist($option);
                }
            } elseif ($obligation->getType() === 'FINE') {
                for ($i=1; $i <= $duree; $i++) {
                    $capitalD =  $investissement;
                    $interetO = $investissement * $interet;
                    $amortiO = $i === $duree ? $capitalD : 0;
                    $annuiteO = round($interetO + $amortiO);
                    $capitalF = $capitalD - $amortiO;
                    $option = new OptionAmortissement();
                    $option
                        ->setPeriode("Année ".$i)
                        ->setCapitalD($capitalD)
                        ->setInteret($interetO)
                        ->setAmorti($amortiO)
                        ->setAnnuite($annuiteO)
                        ->setCapitalF($capitalF)
                        ->setAmortissement($amortissement)
                    ;
                    $em->persist($option);
                }
            } elseif ($obligation->getType() === 'CONSTANTE') {
                //ANNUITEE CONSTANT
            }
            $em->persist($obligation);
            $em->flush();
            $this->addFlash('success', 'Amortissement modifié avec succès.');
            return $this->redirectToRoute('obligation_index');
        }

        return $this->render('obligation/edit.html.twig', [
            'obligation' => $obligation,
            'form' => $form->createView(),
            'title' => "Ajouter une nouvelle obligation",
        ]);
    }

    /**
     * @Route("/{id}", name="obligation_show", methods={"GET"}, 
     * options={"description"="Détails de l'ogligation"})
     */
    public function show(Obligation $obligation): Response
    {
        return $this->render('obligation/show.html.twig', [
            'obligation' => $obligation,
            'title' => "Détails de l'oligation ".$obligation->getLibelle(),
        ]);
    }

    /**
     * @Route("/{id}", name="obligation_delete", methods={"POST"}, 
     * options={"description"="Supprimer une obligation"})
     */
    public function delete(Request $request, Obligation $obligation): Response
    {
        if ($this->isCsrfTokenValid('delete'.$obligation->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($obligation);
            $em->flush();
        }
        
        $this->addFlash('success', 'Amortissement supprimé avec succès.');
        return $this->redirectToRoute('obligation_index');
    }

    
}
