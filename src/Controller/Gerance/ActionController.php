<?php

namespace App\Controller\Gerance;

use App\Entity\Amortissement;
use App\Entity\Filtre;
use App\Entity\Obligation;
use App\Entity\OptionAmortissement;
use App\Form\FiltreType;
use App\Form\ObligationType;
use App\Repository\ObligationRepository;
use App\Services\ParametreService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/private/action")
 */
class ActionController extends AbstractController
{
    private $actionRepository;
    private $param;
    public function __construct(
        ObligationRepository $actionRepository,
        ParametreService $param
    )
    {
        $this->actionRepository = $actionRepository;
        $this->param = $param;
    }

    /**
     * @Route("/", name="action_index", methods={"GET","POST"}, 
     * options={"description"="Liste des actions"})
     */
    public function index(Request $request): Response
    {
        $actions =  $this->actionRepository->findBy([], ['id' => 'DESC'], 10);
        $filtre = new Filtre();
        $form = $this->createForm(FiltreType::class, $filtre, [
            "dateDReq" => true,
            "dateFReq" => true,
            "libelleReq" => true,
            "orderReq" => true,
            "limitReq" => true,
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $actions = $this->actionRepository->findFiltre($filtre);
        }
        return $this->render('action/index.html.twig', [
            'actions' => $actions,
            'form' => $form->createView(),
            'title' => 'Liste des actions',
        ]);
    }

    /**
     * @Route("/new", name="action_new", methods={"GET","POST"}, 
     * options={"description"="Ajouter une nouvelle action"})
     */
    public function new(Request $request): Response
    {
        $action = new Obligation();
        $form = $this->createForm(ObligationType::class, $action);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $interet = $action->getInteret();
            $duree = $action->getDuree();
            $investissement = $action->getMontant();
            $amortissement = new Amortissement();
            $amortissement
                ->setEmetteur($action->getEmetteur())
                ->setAnnee($this->param->annee())
            ;
            $em->persist($amortissement);

            $action
                ->setAnnee($this->param->annee())
                ->setAmortissement($amortissement)
            ;
            $em->persist($action);
            //AMORTISSEMENT CONSTANT
            if ($action->getType() === 'AMORTISMENT') {
                $capitalF = 0;
                for ($i=1; $i <= $duree; $i++) {
                    $capitalD = $i === 1 ? $investissement : $capitalF;
                    $interetO = round($capitalD * $interet);
                    $amortiO = round($investissement / $duree);
                    $annuiteO = round($interetO + $amortiO);
                    $capitalF = $i === $duree ? 0 : $capitalD - $amortiO;
                    $option = new OptionAmortissement();
                    $option
                        ->setPeriode("Année ".$i)
                        ->setCapitalD($capitalD)
                        ->setInteret($interetO)
                        ->setAmorti($amortiO)
                        ->setAnnuite($annuiteO)
                        ->setCapitalF($capitalF)
                        ->setAmortissement($amortissement)
                    ;
                    $em->persist($option);
                }
            } elseif ($action->getType() === 'FINE') {
                for ($i=1; $i <= $duree; $i++) {
                    $capitalD =  $investissement;
                    $interetO = $investissement * $interet;
                    $amortiO = $i === $duree ? $capitalD : 0;
                    $annuiteO = round($interetO + $amortiO);
                    $capitalF = $capitalD - $amortiO;
                    $option = new OptionAmortissement();
                    $option
                        ->setPeriode("Année ".$i)
                        ->setCapitalD($capitalD)
                        ->setInteret($interetO)
                        ->setAmorti($amortiO)
                        ->setAnnuite($annuiteO)
                        ->setCapitalF($capitalF)
                        ->setAmortissement($amortissement)
                    ;
                    $em->persist($option);
                }
            } elseif ($action->getType() === 'CONSTANTE') {
                //ANNUITEE CONSTANT
                for ($i=1; $i <= $duree; $i++) {
                    $capitalD = $i === 1 ? $investissement : 0;
                    $interetO = round($capitalD * $interet);
                    $amortiO = round($investissement / $duree);
                    $annuiteO = round($interetO + $amortiO);
                    $capitalF = $i === $duree ? 0 : $capitalD - $amortiO;
                    $option = new OptionAmortissement();
                    $option
                        ->setPeriode("Année ".$i)
                        ->setCapitalD($capitalD)
                        ->setInteret($interetO)
                        ->setAmorti($amortiO)
                        ->setAnnuite($annuiteO)
                        ->setCapitalF($capitalF)
                        ->setAmortissement($amortissement)
                    ;
                    $em->persist($option);
                }
            }
            dd($action);
            $em->flush();
            $this->addFlash('success', 'Amortissement ajouté avec succès.');
            return $this->redirectToRoute('action_index');
        }

        return $this->render('action/new.html.twig', [
            'action' => $action,
            'form' => $form->createView(),
            'title' => "Ajouter une nouvelle action",
        ]);
    }

    /**
     * @Route("/{id}/edit", name="action_edit", methods={"GET","POST"}, 
     * options={"description"="Modifier une action"})
     */
    public function edit(Obligation $action, Request $request): Response
    {
        $form = $this->createForm(ObligationType::class, $action);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $interet = $action->getInteret();
            $duree = $action->getDuree();
            $investissement = $action->getMontant();
            $amortissement = $action->getAmortissement();
            foreach ($amortissement->getOptions() as $key => $options) {
                $em->remove($options);
            }
            
            //AMORTISSEMENT CONSTANT
            if ($action->getType() === 'AMORTISMENT') {
                $capitalF = 0;
                for ($i=1; $i <= $duree; $i++) {
                    $capitalD = $i === 1 ? $investissement : $capitalF;
                    $interetO = round($capitalD * $interet);
                    $amortiO = round($investissement / $duree);
                    $annuiteO = round($interetO + $amortiO);
                    $capitalF = $i === $duree ? 0 : $capitalD - $amortiO;
                    $option = new OptionAmortissement();
                    $option
                        ->setPeriode("Année ".$i)
                        ->setCapitalD($capitalD)
                        ->setInteret($interetO)
                        ->setAmorti($amortiO)
                        ->setAnnuite($annuiteO)
                        ->setCapitalF($capitalF)
                        ->setAmortissement($amortissement)
                    ;
                    $em->persist($option);
                }
            } elseif ($action->getType() === 'FINE') {
                for ($i=1; $i <= $duree; $i++) {
                    $capitalD =  $investissement;
                    $interetO = $investissement * $interet;
                    $amortiO = $i === $duree ? $capitalD : 0;
                    $annuiteO = round($interetO + $amortiO);
                    $capitalF = $capitalD - $amortiO;
                    $option = new OptionAmortissement();
                    $option
                        ->setPeriode("Année ".$i)
                        ->setCapitalD($capitalD)
                        ->setInteret($interetO)
                        ->setAmorti($amortiO)
                        ->setAnnuite($annuiteO)
                        ->setCapitalF($capitalF)
                        ->setAmortissement($amortissement)
                    ;
                    $em->persist($option);
                }
            } elseif ($action->getType() === 'CONSTANTE') {
                //ANNUITEE CONSTANT
            }
            $em->persist($action);
            $em->flush();
            $this->addFlash('success', 'Amortissement modifié avec succès.');
            return $this->redirectToRoute('action_index');
        }

        return $this->render('action/edit.html.twig', [
            'action' => $action,
            'form' => $form->createView(),
            'title' => "Ajouter une nouvelle action",
        ]);
    }

    /**
     * @Route("/{id}", name="action_show", methods={"GET"}, 
     * options={"description"="Détails de l'ogligation"})
     */
    public function show(Obligation $action): Response
    {
        return $this->render('action/show.html.twig', [
            'action' => $action,
            'title' => "Détails de l'action ".$action->getLibelle(),
        ]);
    }

    /**
     * @Route("/{id}", name="action_delete", methods={"POST"}, 
     * options={"description"="Supprimer une action"})
     */
    public function delete(Request $request, Obligation $action): Response
    {
        if ($this->isCsrfTokenValid('delete'.$action->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($action);
            $em->flush();
        }
        
        $this->addFlash('success', 'Amortissement supprimé avec succès.');
        return $this->redirectToRoute('action_index');
    }
}
