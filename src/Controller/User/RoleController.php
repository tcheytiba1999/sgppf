<?php

namespace App\Controller\User;

use App\Entity\Role;
use App\Entity\Filtre;
use App\Form\FiltreType;
use App\Form\RoleType;
use App\Repository\RoleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/private/role/user")
 */
class RoleController extends AbstractController
{
    private $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * @Route("/", name="role_index", methods={"GET","POST"}, 
     * options={"description"="Liste des groupes d'accès"})
     */
    public function index(Request $request): Response
    {
        $roles =  $this->roleRepository->findBy([], [], 10);
        $filtre = new Filtre();
        $form = $this->createForm(FiltreType::class, $filtre, [
            "dateDReq" => true,
            "dateFReq" => true,
            "libelleReq" => true,
            "orderReq" => true,
            "limitReq" => true,
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $roles = $this->roleRepository->findFiltre($filtre);
        }
        return $this->render('role/index.html.twig', [
            'droits' => $roles,
            'form' => $form->createView(),
            'title' => "Liste des groupes d'accès",
        ]);
    }

    /**
     * @Route("/new", name="role_new", methods={"GET","POST"},
     *  options={"description"="Ajouter un nouveau groupe d'accès"})
     */
    public function new(Request $request): Response
    {
        $role = new Role();
        $form = $this->createForm(RoleType::class, $role);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($role);
            $em->flush();

            $this->addFlash('success', "Groupe d'accès ajouté avec succès.");
            return $this->redirectToRoute('role_index');
        }

        return $this->render('role/new.html.twig', [
            'role' => $role,
            'form' => $form->createView(),
            'title' => "Ajouter un nouveau groupe d'accès",
        ]);
    }

    /**
     * @Route("/{id}/edit", name="role_edit", methods={"GET","POST"},
     *  options={"description"="Modifier un groupe d'accès"})
     */
    public function edit(Request $request, Role $role): Response
    {
        $form = $this->createForm(RoleType::class, $role);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', "Groupe d'accès modifié avec succès.");
            return $this->redirectToRoute('role_index');
        }

        return $this->render('role/edit.html.twig', [
            'role' => $role,
            'form' => $form->createView(),
            'title' => "Modifier le groupe d'accès ".$role->getName(),
        ]);
    }

    /**
     * @Route("/{id}", name="role_delete", methods={"POST"},
     *  options={"description"="Supprimer un groupe d'accès"})
     */
    public function delete(Request $request, Role $role): Response
    {
        if ($this->isCsrfTokenValid('delete'.$role->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($role);
            $em->flush();
        }

        $this->addFlash('success', 'Suppresssion éffectué avec succès.');
        return $this->redirectToRoute('role_index');
    }
}
