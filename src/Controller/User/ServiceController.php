<?php

namespace App\Controller\User;

use App\Entity\Filtre;
use App\Entity\Service;
use App\Form\FiltreType;
use App\Form\ServiceType;
use App\Repository\ServiceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/private/service")
 */
class ServiceController extends AbstractController
{
    private $serviceRepository;

    public function __construct(ServiceRepository $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

    /**
     * @Route("/", name="service_index", methods={"GET","POST"}, options={"Liste des services"})
     */
    public function index(Request $request): Response
    {
        $services =  $this->serviceRepository->findBy([], [], 10);
        $filtre = new Filtre();
        $form = $this->createForm(FiltreType::class, $filtre, [
            "dateDReq" => true,
            "dateFReq" => true,
            "libelleReq" => true,
            "orderReq" => true,
            "limitReq" => true,
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $services = $this->serviceRepository->findFiltre($filtre);
        }
        return $this->render('service/index.html.twig', [
            'services' => $services,
            'title' => 'Liste des services',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="service_new", methods={"GET","POST"}, options={"Ajouter un nouveau service"})
     */
    public function new(Request $request): Response
    {
        $service = new Service();
        $form = $this->createForm(ServiceType::class, $service);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($service);
            $em->flush();

            $this->addFlash('success', 'Service enrégistré avec succès.');
            return $this->redirectToRoute('service_index');
        }

        return $this->render('service/new.html.twig', [
            'service' => $service,
            'form' => $form->createView(),
            'title' => 'Ajouter un nouveau service',
        ]);
    }

    /**
     * @Route("/{id}", name="service_show", methods={"GET"}, options={"Détails du service"})
     */
    public function show(Service $service): Response
    {
        return $this->render('service/show.html.twig', [
            'service' => $service,
            'title' => 'Détails du service '.$service->getLibelle(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="service_edit", methods={"GET","POST"},
     *  options={"Modifier un service"})
     */
    public function edit(Request $request, Service $service): Response
    {
        $form = $this->createForm(ServiceType::class, $service);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Service modifié avec succès.');
            return $this->redirectToRoute('service_index');
        }

        return $this->render('service/edit.html.twig', [
            'service' => $service,
            'form' => $form->createView(),
            'title' => 'Modifier le service '.$service->getLibelle(),
        ]);
    }

    /**
     * @Route("/{id}", name="service_delete", methods={"POST"}, options={"Supprimer un service"})
     */
    public function delete(Request $request, Service $service): Response
    {
        if ($this->isCsrfTokenValid('delete'.$service->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($service);
            $em->flush();
        }

        return $this->redirectToRoute('service_index');
    }
}
