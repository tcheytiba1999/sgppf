<?php

namespace App\Controller\User;

use App\Entity\Filtre;
use App\Entity\SecureRoute;
use App\Form\FiltreType;
use App\Form\SecureRouteType;
use App\Repository\SecureRouteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/route")
 */
class RouteController extends AbstractController
{
    private $secureRouteRepository;

    public function __construct(SecureRouteRepository $secureRouteRepository)
    {
        $this->secureRouteRepository = $secureRouteRepository;
    }

    /**
     * @Route("/", name="route_index", methods={"GET","POST"})
     */
    public function index(Request $request): Response
    {
        $routes =  $this->secureRouteRepository->findBy([], [], 10);
        $filtre = new Filtre();
        $form = $this->createForm(FiltreType::class, $filtre, [
            "dateDReq" => true,
            "dateFReq" => true,
            "libelleReq" => true,
            "orderReq" => true,
            "limitReq" => true,
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $routes = $this->secureRouteRepository->findFiltre($filtre);
        }
        return $this->render('route/index.html.twig', [
            'routes' => $routes,
            'form' => $form->createView(),
            'title' => "Liste des routes",
        ]);
    }

    /**
     * @Route("/{id}/edit", name="route_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SecureRoute $secureRoute): Response
    {
        $form = $this->createForm(SecureRouteType::class, $secureRoute);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('route_index');
        }

        return $this->render('route/edit.html.twig', [
            'secure_route' => $secureRoute,
            'form' => $form->createView(),
        ]);
    }
}
