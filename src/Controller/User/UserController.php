<?php

namespace App\Controller\User;

use App\Entity\Filtre;
use App\Entity\User;
use App\Form\FiltreType;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/private/user")
 */
class UserController extends AbstractController
{
    private $userRepository;
    private $passwordEncoder;

    public function __construct(UserRepository $userRepository, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Route("/", name="user_index", methods={"GET","POST"}, 
     * options={"description"="Liste des utilisateurs"})
     */
    public function index(Request $request): Response
    {
        $users =  $this->userRepository->findBy([], [], 10);
        $filtre = new Filtre();
        $form = $this->createForm(FiltreType::class, $filtre, [
            "dateDReq" => true,
            "dateFReq" => true,
            "libelleReq" => true,
            "orderReq" => true,
            "limitReq" => true,
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $users = $this->userRepository->findFiltre($filtre);
        }
        return $this->render('user/index.html.twig', [
            'users' => $users,
            'form' => $form->createView(),
            'connecte' => false,
            'title' => 'Liste des utilisateurs',
        ]);
    }

    /**
     * @Route("/new", name="user_new", methods={"GET","POST"}, options={"Ajouter un nouveau utilisateur"})
     */
    public function new(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $password = $this->passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', 'Utilisateur ajouté avec succès.');
            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'title' => 'Ajouter un nouvel utilisateur'
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"}, options={"Détails d'un utilisateur"})
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
            'title' => "Détails de l'utilisateur ".$user->__toString()
        ]);
    }

    /**
     * @Route("/profil/user", name="user_profil", methods={"GET"},
     *  options={"description"="Détails service"})
     */
    public function profil(): Response
    {
        return $this->render('user/profil.html.twig', [
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"}, options={"Modifier un utilisateurs"})
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user, [
            'passwordReq' => false
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'title' => "Modiflier l'utilisateur ".$user->__toString()
        ]);
    }

    /**
     * @Route("/{id}/edit/profil", name="user_edit_profil", methods={"GET","POST"},
     *  options={"description"="Modifier votre profil utilisateur"})
     */
    public function editProfil(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user, [ 
            "passwordReq" => false,
            "groupeReq" => false,
            "userRolesReq" => false
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Utilisateur modifié avec succès.');
            return $this->redirectToRoute('user_profil');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'title' => "Modiflier votre profil utilisateur ".$user->__toString()
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"POST"}, options={"Supprimer un utilisateur"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
        }

        return $this->redirectToRoute('user_index');
    }
}
