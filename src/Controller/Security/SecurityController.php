<?php

namespace App\Controller\Security;

use App\Entity\User;
use App\Form\ResetPasswordType;
use App\Repository\UserRepository;
use Symfony\Component\Form\FormError;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class SecurityController extends AbstractController
{
    public $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils, Request $request): Response
    {
        $id = $request->query->get('id');
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        $errorMsg = null;
        $success = null;

        if ($id == 1) {
            $errorMsg = "Nom utilisateur n'existe pas";
        } elseif ($id == 2) {
            $success = "Un mail vous a été envoyé pour la demande de mot de passe oublié";
        }
        
        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error, 'errorMsg' => $errorMsg, 'success' => $success]);
    }

    /**
     * @Route("/forgot/password", name="forgot_password", methods={"GET","POST"},
     *  options={"description"="Mot de passe oublié"})
     */
    public function forgetPassword(Request $request)
    {
        dd('Je suis là');
        if ($request->isMethod('POST')) {
            // ENVOI D'SMS A L'UTILISATEUR A PARTIR DE SON MAIL.
            dd('Je suis là');
        }

        return $this->render('security/login.html.twig', [
        ]);
    }

    /**
     * @Route("/privated/forgot/password/{id}", name="forgot_password_user",
     *  options={"description"="Changer votre mot de passe"})
     */
    public function forgotPasswordUser(?User $user, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $oldPassword = $form->get('oldPassword')->getData();
            $newPassword = $form->get('password')->getData();

            // Si l'ancien mot de passe est bon
            if ($passwordEncoder->isPasswordValid($user, $oldPassword)) {
                $newEncodedPassword = $passwordEncoder->encodePassword($user, $newPassword);
                $user->setPassword($newEncodedPassword);
                $em->persist($user);
                $em->flush();

                $this->addFlash('success', 'Votre mot de passe à bien été changé !');
                return $this->redirectToRoute('user_profil');
            } else {
                $form->addError(new FormError('Ancien mot de passe incorrect'));
            }
        }
    	
    	return $this->render('user/password.html.twig', [
    		'form' => $form->createView(),
    		'user' => $user,
    		'title' => "Modifier mon mot de passe",
    	]);
    }

    /**
     * @Route("/privated/reset/{id}/password", name="reset_password",
     *  options={"description"="Réinitialiser le mot de passe"})
     */
    public function resetPassword(User $user, UserPasswordEncoderInterface $passwordEncoder)
    {
        $em = $this->getDoctrine()->getManager();
        $newEncodedPassword = $passwordEncoder->encodePassword($user, "mypassword");
        $user->setPassword($newEncodedPassword);
        $em->persist($user);
        $em->flush();

        $this->addFlash('success', 'Le mot de passe à bien été réinitialisé ! votre nouveau mot de passe est "mypassword". Il est recommandé de le changer cas ce mot de passe est STANDARD.');
        return $this->redirectToRoute('user_index');
    }
}


