<?php

namespace App\Controller\Setting;

use App\Entity\Filtre;
use App\Entity\Emetteur;
use App\Form\FiltreType;
use App\Form\EmetteurType;
use App\Repository\EmetteurRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/private/emetteur")
 */
class EmetteurController extends AbstractController
{
    private $emetteurRepository;
    public function __construct(EmetteurRepository $emetteurRepository)
    {
        $this->emetteurRepository = $emetteurRepository;
    }
    
    /**
     * @Route("/", name="emetteur_index", methods={"GET"}, 
     * options={"description"="Liste des emetteurs"})
     */
    public function index(Request $request): Response
    {
        $emetteurs =  $this->emetteurRepository->findBy([], ['id' => 'DESC'], 10);
        $filtre = new Filtre();
        $form = $this->createForm(FiltreType::class, $filtre, [
            "dateDReq" => true,
            "dateFReq" => true,
            "libelleReq" => true,
            "orderReq" => true,
            "limitReq" => true,
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $emetteurs = $this->emetteurRepository->findFiltre($filtre);
        }
        return $this->render('emetteur/index.html.twig', [
            'emetteurs' => $emetteurs,
            'form' => $form->createView(),
            'title' => 'Liste des emetteurs',
        ]);
    }

    /**
     * @Route("/new", name="emetteur_new", methods={"GET","POST"}, 
     * options={"description"="Ajouter un nouveau emetteur"})
     */
    public function new(Request $request): Response
    {
        $emetteur = new Emetteur();
        $form = $this->createForm(EmetteurType::class, $emetteur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($emetteur);
            $em->flush();

            return $this->redirectToRoute('emetteur_index');
        }

        return $this->render('emetteur/new.html.twig', [
            'emetteur' => $emetteur,
            'form' => $form->createView(),
            'title' => 'Ajouer un nouveau emetteur',
        ]);
    }

    /**
     * @Route("/{id}", name="emetteur_show", methods={"GET"}, 
     * options={"description"="Détails d'un emetteur"})
     */
    public function show(Emetteur $emetteur): Response
    {
        return $this->render('emetteur/show.html.twig', [
            'emetteur' => $emetteur,
            'title' => "Détails de l'emetteur ".$emetteur->getNom()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="emetteur_edit", methods={"GET","POST"}, 
     * options={"description"="Modifier un emetteur"})
     */
    public function edit(Request $request, Emetteur $emetteur): Response
    {
        $form = $this->createForm(EmetteurType::class, $emetteur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('emetteur_index');
        }

        return $this->render('emetteur/edit.html.twig', [
            'emetteur' => $emetteur,
            'form' => $form->createView(),
            'title' => "Modifier l'emetteur ".$emetteur->getNom()
        ]);
    }

    /**
     * @Route("/{id}", name="emetteur_delete", methods={"POST"}, 
     * options={"description"="Supprimer un emetteur"})
     */
    public function delete(Request $request, Emetteur $emetteur): Response
    {
        if ($this->isCsrfTokenValid('delete'.$emetteur->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($emetteur);
            $em->flush();
        }

        return $this->redirectToRoute('emetteur_index');
    }
}
