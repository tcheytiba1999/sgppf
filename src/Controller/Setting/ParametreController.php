<?php

namespace App\Controller\Setting;

use App\Entity\Parametre;
use App\Form\ParametreType;
use App\Repository\ParametreRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/private/parametre")
 */
class ParametreController extends AbstractController
{
    private $parametreRepository;
    public function __construct(ParametreRepository $parametreRepository)
    {
        $this->parametreRepository = $parametreRepository;
    }

    /**
     * @Route("/new", name="parametre_new", methods={"GET","POST"}, 
     * options={"description"="Modifier parametre général"})
     */
    public function new(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $param = $this->parametreRepository->find(1);
        $parametre = ($param) ? $param : new Parametre();
        $form = $this->createForm(ParametreType::class, $parametre);
        $form->handleRequest($request);
        if ($request->isMethod('POST')) {
            $em->persist($parametre);
            $em->flush();
            $this->addFlash('success', 'Configuration enrégistré avec succès');
            return $this->redirectToRoute('home_parametre');
        }

        return $this->render('parametre/new.html.twig', [
            'parametre' => $parametre,
            'form' => $form->createView(),
            'title' => "Modifier parametre général"
        ]);
    }
}
