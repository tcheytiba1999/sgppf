<?php

namespace App\Controller\Setting;

use App\Entity\Annee;
use App\Entity\Filtre;
use App\Form\AnneeType;
use App\Form\FiltreType;
use App\Repository\AnneeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/private/annee")
 */
class AnneeController extends AbstractController
{
    private $anneeRepository;
    public function __construct(AnneeRepository $anneeRepository)
    {
        $this->anneeRepository = $anneeRepository;
    }

    /**
     * @Route("/", name="annee_index", methods={"GET","POST"}, 
     * options={"description"="Liste des années"})
     */
    public function index(Request $request): Response
    {
        $annees =  $this->anneeRepository->findBy([], ['id' => 'DESC'], 10);
        $filtre = new Filtre();
        $form = $this->createForm(FiltreType::class, $filtre, [
            "dateDReq" => true,
            "dateFReq" => true,
            "libelleReq" => true,
            "orderReq" => true,
            "limitReq" => true,
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $annees = $this->anneeRepository->findFiltre($filtre);
        }
        return $this->render('annee/index.html.twig', [
            'annees' => $annees,
            'form' => $form->createView(),
            'title' => 'Liste des années',
        ]);
    }

    /**
     * @Route("/new", name="annee_new", methods={"GET","POST"}, 
     * options={"description"="Ajouter une nouvelle année"})
     */
    public function new(Request $request): Response
    {
        $annee = new Annee();
        $form = $this->createForm(AnneeType::class, $annee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $check = $this->anneeRepository->findOneById(1);
            if (!$check) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($annee);
                $em->flush();
                $this->addFlash('success', "L'année a été enrégistrée avec succès");
                return $this->redirectToRoute('annee_index');
            } else {
                $this->addFlash('warning', "Vous ne pouvez pas ajouter une nouvelle année sans avoir cloturé la précédente.");
            }
        }

        return $this->render('annee/new.html.twig', [
            'annee' => $annee,
            'form' => $form->createView(),
            'title' => 'Ajouter une nouvelle année'
        ]);
    }

    /**
     * @Route("/{id}", name="annee_show", methods={"GET"}, 
     * options={"description"="Liste des SGI"})
     */
    public function show(Annee $annee): Response
    {
        return $this->render('annee/show.html.twig', [
            'annee' => $annee,
            'title' => "Détails l'année ".$annee->getLibelle()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="annee_edit", methods={"GET","POST"}, 
     * options={"description"="Modifier une année"})
     */
    public function edit(Request $request, Annee $annee): Response
    {
        $form = $this->createForm(AnneeType::class, $annee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('annee_index');
        }

        return $this->render('annee/edit.html.twig', [
            'annee' => $annee,
            'form' => $form->createView(),
            'title' => "Modifier l'année ".$annee->getLibelle()
        ]);
    }

    /**
     * @Route("/{id}", name="annee_delete", methods={"POST"}, 
     * options={"description"="Supprimer une année"})
     */
    public function delete(Request $request, Annee $annee): Response
    {
        if ($this->isCsrfTokenValid('delete'.$annee->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($annee);
            $em->flush();
        }

        return $this->redirectToRoute('annee_index');
    }
}
