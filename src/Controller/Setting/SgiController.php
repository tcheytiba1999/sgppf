<?php

namespace App\Controller\Setting;

use App\Entity\Sgi;
use App\Form\SgiType;
use App\Entity\Filtre;
use App\Form\FiltreType;
use App\Repository\SgiRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/private/sgi")
 */
class SgiController extends AbstractController
{
    private $sgiRepository;
    public function __construct(SgiRepository $sgiRepository)
    {
        $this->sgiRepository = $sgiRepository;
    }

    /**
     * @Route("/", name="sgi_index", methods={"GET","POST"}, 
     * options={"description"="Liste des SGI"})
     */
    public function index(Request $request): Response
    {
        $sgis =  $this->sgiRepository->findBy([], ['id' => 'DESC'], 10);
        $filtre = new Filtre();
        $form = $this->createForm(FiltreType::class, $filtre, [
            "dateDReq" => true,
            "dateFReq" => true,
            "libelleReq" => true,
            "orderReq" => true,
            "limitReq" => true,
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $sgis = $this->sgiRepository->findFiltre($filtre);
        }
        return $this->render('sgi/index.html.twig', [
            'sgis' => $sgis,
            'form' => $form->createView(),
            'title' => 'Liste des SGI',
        ]);
    }

    /**
     * @Route("/new", name="sgi_new", methods={"GET","POST"}, 
     * options={"description"="Ajouter un nouveau SGI"})
     */
    public function new(Request $request): Response
    {
        $sgi = new Sgi();
        $form = $this->createForm(SgiType::class, $sgi);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($sgi);
            $em->flush();
            $this->addFlash('success', 'SGI enrégistré avec succès');
            return $this->redirectToRoute('sgi_index');
        }

        return $this->render('sgi/new.html.twig', [
            'sgi' => $sgi,
            'form' => $form->createView(),
            'title' => "Ajouter un nouveau SGI"
        ]);
    }

    /**
     * @Route("/{id}", name="sgi_show", methods={"GET"}, 
     * options={"description"="Détails d'un SGI"})
     */
    public function show(Sgi $sgi): Response
    {
        return $this->render('sgi/show.html.twig', [
            'sgi' => $sgi,
            'title' => "Dateils du SGI ".$sgi->getNom()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="sgi_edit", methods={"GET","POST"}, 
     * options={"description"="Modifier un SGI"})
     */
    public function edit(Request $request, Sgi $sgi): Response
    {
        $form = $this->createForm(SgiType::class, $sgi);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sgi_index');
        }

        return $this->render('sgi/edit.html.twig', [
            'sgi' => $sgi,
            'form' => $form->createView(),
            'title' => "Modifier le SGI ".$sgi->getNom()
        ]);
    }

    /**
     * @Route("/{id}", name="sgi_delete", methods={"POST"}, 
     * options={"description"="Supprimer un SGI"})
     */
    public function delete(Request $request, Sgi $sgi): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sgi->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($sgi);
            $entityManager->flush();
        }

        return $this->redirectToRoute('sgi_index');
    }
}
