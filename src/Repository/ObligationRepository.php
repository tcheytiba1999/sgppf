<?php

namespace App\Repository;

use App\Entity\Obligation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Obligation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Obligation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Obligation[]    findAll()
 * @method Obligation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObligationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Obligation::class);
    }

    /**
     * Filtre des Obligation
     * Les parametres sont dans l'ordre suivant : (Requete du filtre)
     * @return array
     */
    public function findFiltre($filtre): array
    {
        $query = $this->createQueryBuilder('o');
        if ($filtre->getLibelle()) {
            $query = $query
                ->andWhere('o.libelle LIKE :libelle')
                ->setParameter('libelle', '%'.$filtre->getLibelle().'%')
            ;
        }
        if ($filtre->getDateD()) {
            $query = $query
                ->andWhere('o.date >= :dateD')
                ->setParameter('dateD', $filtre->getDateD())
            ;
        }
        if ($filtre->getDateF()) {
            $query = $query
                ->andWhere('o.date <= :dateF')
                ->setParameter('dateF', $filtre->getDateF())
            ;
        }

        return ($filtre->getLimit()) ? $query->orderBy('o.id', $filtre->getOrder())->setMaxResults($filtre->getLimit())->getQuery()->getResult() : 
            $query->orderBy('o.id', $filtre->getOrder())->getQuery()->getResult();
    }

    // /**
    //  * @return Obligation[] Returns an array of Obligation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Obligation
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
