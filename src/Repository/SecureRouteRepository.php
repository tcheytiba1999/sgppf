<?php

namespace App\Repository;

use App\Entity\SecureRoute;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SecureRoute|null find($id, $lockMode = null, $lockVersion = null)
 * @method SecureRoute|null findOneBy(array $criteria, array $orderBy = null)
 * @method SecureRoute[]    findAll()
 * @method SecureRoute[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SecureRouteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SecureRoute::class);
    }

    /**
     * Filtre des groupes de role
     * Les parametres sont dans l'ordre suivant : (Requete du filtre)
     * @return array
     */
    public function findFiltre($filtre): array
    {
        $query = $this->createQueryBuilder('s');
        if ($filtre->getLibelle()) {
            $query = $query
                ->andWhere('s.description LIKE :description')
                ->setParameter('description', '%'.$filtre->getLibelle().'%')
            ;
        }
        if ($filtre->getDateD()) {
            $query = $query
                ->andWhere('s.createdAt >= :dateD')
                ->setParameter('dateD', $filtre->getDateD())
            ;
        }
        if ($filtre->getDateF()) {
            $query = $query
                ->andWhere('s.createdAt <= :dateF')
                ->setParameter('dateF', $filtre->getDateF())
            ;
        }

        return ($filtre->getLimit()) ? $query->orderBy('s.id', $filtre->getOrder())->setMaxResults($filtre->getLimit())->getQuery()->getResult() : 
            $query->orderBy('s.id', $filtre->getOrder())->getQuery()->getResult();
    }

    // /**
    //  * @return SecureRoute[] Returns an array of SecureRoute objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SecureRoute
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
