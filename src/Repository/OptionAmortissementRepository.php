<?php

namespace App\Repository;

use App\Entity\OptionAmortissement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OptionAmortissement|null find($id, $lockMode = null, $lockVersion = null)
 * @method OptionAmortissement|null findOneBy(array $criteria, array $orderBy = null)
 * @method OptionAmortissement[]    findAll()
 * @method OptionAmortissement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OptionAmortissementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OptionAmortissement::class);
    }

    // /**
    //  * @return OptionAmortissement[] Returns an array of OptionAmortissement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OptionAmortissement
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
