<?php

namespace App\Repository;

use App\Entity\Sgi;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Sgi|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sgi|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sgi[]    findAll()
 * @method Sgi[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SgiRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sgi::class);
    }

    /**
     * Filtre des SGI
     * Les parametres sont dans l'ordre suivant : (Requete du filtre)
     * @return array
     */
    public function findFiltre($filtre): array
    {
        $query = $this->createQueryBuilder('s');
        if ($filtre->getLibelle()) {
            $query = $query
                ->andWhere('s.nom LIKE :nom')
                ->setParameter('nom', '%'.$filtre->getLibelle().'%')
            ;
        }
        if ($filtre->getDateD()) {
            $query = $query
                ->andWhere('s.createdAt >= :dateD')
                ->setParameter('dateD', $filtre->getDateD())
            ;
        }
        if ($filtre->getDateF()) {
            $query = $query
                ->andWhere('s.createdAt <= :dateF')
                ->setParameter('dateF', $filtre->getDateF())
            ;
        }

        return ($filtre->getLimit()) ? $query->orderBy('s.id', $filtre->getOrder())->setMaxResults($filtre->getLimit())->getQuery()->getResult() : 
            $query->orderBy('s.id', $filtre->getOrder())->getQuery()->getResult();
    }

    // /**
    //  * @return Sgi[] Returns an array of Sgi objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Sgi
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
