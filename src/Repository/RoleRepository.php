<?php

namespace App\Repository;

use App\Entity\Role;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Role|null find($id, $lockMode = null, $lockVersion = null)
 * @method Role|null findOneBy(array $criteria, array $orderBy = null)
 * @method Role[]    findAll()
 * @method Role[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Role::class);
    }

    /**
     * Filtre des groupes de role
     * Les parametres sont dans l'ordre suivant : (Requete du filtre)
     * @return array
     */
    public function findFiltre($filtre): array
    {
        $query = $this->createQueryBuilder('r');
        if ($filtre->getLibelle()) {
            $query = $query
                ->andWhere('r.name LIKE :name')
                ->setParameter('name', '%'.$filtre->getLibelle().'%')
            ;
        }
        if ($filtre->getDateD()) {
            $query = $query
                ->andWhere('r.createdAt >= :dateD')
                ->setParameter('dateD', $filtre->getDateD())
            ;
        }
        if ($filtre->getDateF()) {
            $query = $query
                ->andWhere('r.createdAt <= :dateF')
                ->setParameter('dateF', $filtre->getDateF())
            ;
        }

        return ($filtre->getLimit()) ? $query->orderBy('r.id', $filtre->getOrder())->setMaxResults($filtre->getLimit())->getQuery()->getResult() : 
            $query->orderBy('r.id', $filtre->getOrder())->getQuery()->getResult();
    }

    // /**
    //  * @return Role[] Returns an array of Role objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Role
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
