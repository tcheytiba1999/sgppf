<?php

namespace App\Repository;

use App\Entity\Annee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Annee|null find($id, $lockMode = null, $lockVersion = null)
 * @method Annee|null findOneBy(array $criteria, array $orderBy = null)
 * @method Annee[]    findAll()
 * @method Annee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnneeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Annee::class);
    }

    /**
     * Filtre des Annee
     * Les parametres sont dans l'ordre suivant : (Requete du filtre)
     * @return array
     */
    public function findFiltre($filtre): array
    {
        $query = $this->createQueryBuilder('a');
        if ($filtre->getLibelle()) {
            $query = $query
                ->andWhere('a.libelle LIKE :libelle')
                ->setParameter('libelle', '%'.$filtre->getLibelle().'%')
            ;
        }
        if ($filtre->getDateD()) {
            $query = $query
                ->andWhere('a.createdAt >= :dateD')
                ->setParameter('dateD', $filtre->getDateD())
            ;
        }
        if ($filtre->getDateF()) {
            $query = $query
                ->andWhere('a.createdAt <= :dateF')
                ->setParameter('dateF', $filtre->getDateF())
            ;
        }

        return ($filtre->getLimit()) ? $query->orderBy('a.id', $filtre->getOrder())->setMaxResults($filtre->getLimit())->getQuery()->getResult() : 
            $query->orderBy('a.id', $filtre->getOrder())->getQuery()->getResult();
    }

    // /**
    //  * @return Annee[] Returns an array of Annee objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Annee
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
