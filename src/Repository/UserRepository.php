<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Filtre des utilisateurs
     * Les parametres sont dans l'ordre suivant : (Requete du filtre)
     * @return array
     */
    public function findFiltre($filtre): array
    {
        $query = $this->createQueryBuilder('u')
            ->where('u.first = :first')
            ->setParameter('first', false)
        ;
        if ($filtre->getLibelle()) {
            $query = $query
                ->andWhere('u.nom LIKE :nom')
                ->setParameter('nom', '%'.$filtre->getLibelle().'%')
                ->orWhere('u.prenom LIKE :prenom')
                ->setParameter('prenom', '%'.$filtre->getLibelle().'%')
            ;
        }
        if ($filtre->getDateD()) {
            $query = $query
                ->andWhere('u.createdAt >= :dateD')
                ->setParameter('dateD', $filtre->getDateD())
            ;
        }
        if ($filtre->getDateF()) {
            $query = $query
                ->andWhere('u.createdAt <= :dateF')
                ->setParameter('dateF', $filtre->getDateF())
            ;
        }

        return ($filtre->getLimit()) ? $query->orderBy('u.id', $filtre->getOrder())->setMaxResults($filtre->getLimit())->getQuery()->getResult() : 
            $query->orderBy('u.id', $filtre->getOrder())->getQuery()->getResult();
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
