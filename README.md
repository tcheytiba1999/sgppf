PROCESUS D'INTEGRATION

----- CONCERNANT LA RECUPERATION DU PROJET -----

1 - Faire un Fork de la branche principale
2 - Faire un clone depuis votre dépôt en ligne

----- CONCERNANT L'INSTALLATION DE VOTRE ENVIRONNMENT -----

1 - Faire les commandes qui suive
    * composer install
    * npm i ou yarn add (En fonction de votre installation)
    * php bin/console doctrine:database:create
    * php bin/console doc:sch:up --force
    * php bin/console doctrine:fixtures:load

NB : Faire les commandes "composer" et "npm" signifie que vous avez déjà installé composer et node.js sur votre machine. Si c'est pas le cas, alors faites le avant.


----- CONCERNANT LE LANCEMENT DE L'APPLICATION -----

1 - Faire les commandes qui suive
    * symfony server:start
    * npm run dev-server
NB : Faire les commandes "symfony" signifie que vous avez déjà installé symfony sur votre machine. Si c'est pas le cas, alors faites le avant. Le principe c'est de lancer les deux commandes, dans deux terminaux différents.



git remote add upstream https://gitlab.com/anicet.gnahoua/sgpp.git


recuperer
git pull --rebase upstream master