<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210501151621 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE obligation (id INT AUTO_INCREMENT NOT NULL, create_by_id INT DEFAULT NULL, update_by_id INT DEFAULT NULL, remove_by_id INT DEFAULT NULL, libelle VARCHAR(255) NOT NULL, emetteur VARCHAR(255) NOT NULL, duree INT NOT NULL, interet DOUBLE PRECISION NOT NULL, nominal INT NOT NULL, montant DOUBLE PRECISION NOT NULL, amortissement VARCHAR(255) NOT NULL, grace DATE NOT NULL, nbr INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_720EBF279E085865 (create_by_id), INDEX IDX_720EBF27CA83C286 (update_by_id), INDEX IDX_720EBF271748B4A5 (remove_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE obligation ADD CONSTRAINT FK_720EBF279E085865 FOREIGN KEY (create_by_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE obligation ADD CONSTRAINT FK_720EBF27CA83C286 FOREIGN KEY (update_by_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE obligation ADD CONSTRAINT FK_720EBF271748B4A5 FOREIGN KEY (remove_by_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE groupe ADD create_by_id INT DEFAULT NULL, ADD update_by_id INT DEFAULT NULL, ADD remove_by_id INT DEFAULT NULL, ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE groupe ADD CONSTRAINT FK_4B98C219E085865 FOREIGN KEY (create_by_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE groupe ADD CONSTRAINT FK_4B98C21CA83C286 FOREIGN KEY (update_by_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE groupe ADD CONSTRAINT FK_4B98C211748B4A5 FOREIGN KEY (remove_by_id) REFERENCES `user` (id)');
        $this->addSql('CREATE INDEX IDX_4B98C219E085865 ON groupe (create_by_id)');
        $this->addSql('CREATE INDEX IDX_4B98C21CA83C286 ON groupe (update_by_id)');
        $this->addSql('CREATE INDEX IDX_4B98C211748B4A5 ON groupe (remove_by_id)');
        $this->addSql('ALTER TABLE role DROP FOREIGN KEY FK_57698A6AD5766755');
        $this->addSql('ALTER TABLE role DROP FOREIGN KEY FK_57698A6AEEFE5067');
        $this->addSql('DROP INDEX IDX_57698A6AEEFE5067 ON role');
        $this->addSql('DROP INDEX IDX_57698A6AD5766755 ON role');
        $this->addSql('ALTER TABLE role ADD create_by_id INT DEFAULT NULL, ADD update_by_id INT DEFAULT NULL, ADD remove_by_id INT DEFAULT NULL, DROP user_create_id, DROP user_update_id, CHANGE created_at created_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE role ADD CONSTRAINT FK_57698A6A9E085865 FOREIGN KEY (create_by_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE role ADD CONSTRAINT FK_57698A6ACA83C286 FOREIGN KEY (update_by_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE role ADD CONSTRAINT FK_57698A6A1748B4A5 FOREIGN KEY (remove_by_id) REFERENCES `user` (id)');
        $this->addSql('CREATE INDEX IDX_57698A6A9E085865 ON role (create_by_id)');
        $this->addSql('CREATE INDEX IDX_57698A6ACA83C286 ON role (update_by_id)');
        $this->addSql('CREATE INDEX IDX_57698A6A1748B4A5 ON role (remove_by_id)');
        $this->addSql('ALTER TABLE secure_route ADD create_by_id INT DEFAULT NULL, ADD update_by_id INT DEFAULT NULL, ADD remove_by_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE secure_route ADD CONSTRAINT FK_87A30E119E085865 FOREIGN KEY (create_by_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE secure_route ADD CONSTRAINT FK_87A30E11CA83C286 FOREIGN KEY (update_by_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE secure_route ADD CONSTRAINT FK_87A30E111748B4A5 FOREIGN KEY (remove_by_id) REFERENCES `user` (id)');
        $this->addSql('CREATE INDEX IDX_87A30E119E085865 ON secure_route (create_by_id)');
        $this->addSql('CREATE INDEX IDX_87A30E11CA83C286 ON secure_route (update_by_id)');
        $this->addSql('CREATE INDEX IDX_87A30E111748B4A5 ON secure_route (remove_by_id)');
        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD2D5766755');
        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD2EEFE5067');
        $this->addSql('DROP INDEX IDX_E19D9AD2EEFE5067 ON service');
        $this->addSql('DROP INDEX IDX_E19D9AD2D5766755 ON service');
        $this->addSql('ALTER TABLE service ADD create_by_id INT DEFAULT NULL, ADD update_by_id INT DEFAULT NULL, ADD remove_by_id INT DEFAULT NULL, DROP user_create_id, DROP user_update_id, CHANGE created_at created_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD29E085865 FOREIGN KEY (create_by_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD2CA83C286 FOREIGN KEY (update_by_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD21748B4A5 FOREIGN KEY (remove_by_id) REFERENCES `user` (id)');
        $this->addSql('CREATE INDEX IDX_E19D9AD29E085865 ON service (create_by_id)');
        $this->addSql('CREATE INDEX IDX_E19D9AD2CA83C286 ON service (update_by_id)');
        $this->addSql('CREATE INDEX IDX_E19D9AD21748B4A5 ON service (remove_by_id)');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649D5766755');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649EEFE5067');
        $this->addSql('DROP INDEX IDX_8D93D649D5766755 ON user');
        $this->addSql('DROP INDEX IDX_8D93D649EEFE5067 ON user');
        $this->addSql('ALTER TABLE user ADD create_by_id INT DEFAULT NULL, ADD update_by_id INT DEFAULT NULL, ADD remove_by_id INT DEFAULT NULL, DROP user_create_id, DROP user_update_id, CHANGE created_at created_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6499E085865 FOREIGN KEY (create_by_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649CA83C286 FOREIGN KEY (update_by_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6491748B4A5 FOREIGN KEY (remove_by_id) REFERENCES `user` (id)');
        $this->addSql('CREATE INDEX IDX_8D93D6499E085865 ON user (create_by_id)');
        $this->addSql('CREATE INDEX IDX_8D93D649CA83C286 ON user (update_by_id)');
        $this->addSql('CREATE INDEX IDX_8D93D6491748B4A5 ON user (remove_by_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE obligation');
        $this->addSql('ALTER TABLE groupe DROP FOREIGN KEY FK_4B98C219E085865');
        $this->addSql('ALTER TABLE groupe DROP FOREIGN KEY FK_4B98C21CA83C286');
        $this->addSql('ALTER TABLE groupe DROP FOREIGN KEY FK_4B98C211748B4A5');
        $this->addSql('DROP INDEX IDX_4B98C219E085865 ON groupe');
        $this->addSql('DROP INDEX IDX_4B98C21CA83C286 ON groupe');
        $this->addSql('DROP INDEX IDX_4B98C211748B4A5 ON groupe');
        $this->addSql('ALTER TABLE groupe DROP create_by_id, DROP update_by_id, DROP remove_by_id, DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE role DROP FOREIGN KEY FK_57698A6A9E085865');
        $this->addSql('ALTER TABLE role DROP FOREIGN KEY FK_57698A6ACA83C286');
        $this->addSql('ALTER TABLE role DROP FOREIGN KEY FK_57698A6A1748B4A5');
        $this->addSql('DROP INDEX IDX_57698A6A9E085865 ON role');
        $this->addSql('DROP INDEX IDX_57698A6ACA83C286 ON role');
        $this->addSql('DROP INDEX IDX_57698A6A1748B4A5 ON role');
        $this->addSql('ALTER TABLE role ADD user_create_id INT DEFAULT NULL, ADD user_update_id INT DEFAULT NULL, DROP create_by_id, DROP update_by_id, DROP remove_by_id, CHANGE created_at created_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE role ADD CONSTRAINT FK_57698A6AD5766755 FOREIGN KEY (user_update_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE role ADD CONSTRAINT FK_57698A6AEEFE5067 FOREIGN KEY (user_create_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_57698A6AEEFE5067 ON role (user_create_id)');
        $this->addSql('CREATE INDEX IDX_57698A6AD5766755 ON role (user_update_id)');
        $this->addSql('ALTER TABLE secure_route DROP FOREIGN KEY FK_87A30E119E085865');
        $this->addSql('ALTER TABLE secure_route DROP FOREIGN KEY FK_87A30E11CA83C286');
        $this->addSql('ALTER TABLE secure_route DROP FOREIGN KEY FK_87A30E111748B4A5');
        $this->addSql('DROP INDEX IDX_87A30E119E085865 ON secure_route');
        $this->addSql('DROP INDEX IDX_87A30E11CA83C286 ON secure_route');
        $this->addSql('DROP INDEX IDX_87A30E111748B4A5 ON secure_route');
        $this->addSql('ALTER TABLE secure_route DROP create_by_id, DROP update_by_id, DROP remove_by_id');
        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD29E085865');
        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD2CA83C286');
        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD21748B4A5');
        $this->addSql('DROP INDEX IDX_E19D9AD29E085865 ON service');
        $this->addSql('DROP INDEX IDX_E19D9AD2CA83C286 ON service');
        $this->addSql('DROP INDEX IDX_E19D9AD21748B4A5 ON service');
        $this->addSql('ALTER TABLE service ADD user_create_id INT DEFAULT NULL, ADD user_update_id INT DEFAULT NULL, DROP create_by_id, DROP update_by_id, DROP remove_by_id, CHANGE created_at created_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD2D5766755 FOREIGN KEY (user_update_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD2EEFE5067 FOREIGN KEY (user_create_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_E19D9AD2EEFE5067 ON service (user_create_id)');
        $this->addSql('CREATE INDEX IDX_E19D9AD2D5766755 ON service (user_update_id)');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D6499E085865');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D649CA83C286');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D6491748B4A5');
        $this->addSql('DROP INDEX IDX_8D93D6499E085865 ON `user`');
        $this->addSql('DROP INDEX IDX_8D93D649CA83C286 ON `user`');
        $this->addSql('DROP INDEX IDX_8D93D6491748B4A5 ON `user`');
        $this->addSql('ALTER TABLE `user` ADD user_create_id INT DEFAULT NULL, ADD user_update_id INT DEFAULT NULL, DROP create_by_id, DROP update_by_id, DROP remove_by_id, CHANGE created_at created_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE `user` ADD CONSTRAINT FK_8D93D649D5766755 FOREIGN KEY (user_update_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE `user` ADD CONSTRAINT FK_8D93D649EEFE5067 FOREIGN KEY (user_create_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_8D93D649D5766755 ON `user` (user_update_id)');
        $this->addSql('CREATE INDEX IDX_8D93D649EEFE5067 ON `user` (user_create_id)');
    }
}
