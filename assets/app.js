/**
 * MES FICHIERS CSS
 */
import './styles/app.css';

/**
 * MES FICHIERS JS
 */
import $ from 'jquery';
import 'popper.js/dist/umd/popper';
import 'bootstrap';
import 'datatables.net-bs4';
import 'chart.js/dist/Chart.min.js';
import '@grafikart/drop-files-element';
import feather from 'feather-icons/dist/feather';

/**
 * FICHIERS IMPORTE
 */
import './bootstrap';
import Plugins from './js/plugins';
import DataTable from './js/datatable';
import FormObligation from './js/formObligation';

/**
 * APPEL
 */
feather.replace({
    class: 'feather feather-x',
    xmlns: 'http://www.w3.org/2000/svg',
    width: 22,
    height: 22,
    viewBox: '0 0 24 24',
    fill: 'none',
    stroke: 'currentColor',
    'stroke-width': 1.5,
    'stroke-linecap': 'round',
    'stroke-linejoin': 'round',
})
Plugins.init();
DataTable.init();
FormObligation.init();
