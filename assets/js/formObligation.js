export default class FormObligation {
    static init (){
        const $ = require('jquery');
        $(document).ready(function() {
            var nbr = $('.souscrit').val();
            var montant = 0;
            
            $('.souscrit').change(function(){
                var souscrit = $(this).val();
                var nominal = $('.nominal').val();
                nbr = (souscrit !== undefined && souscrit !== '') ? parseInt(souscrit) : 0;
                nominal = (nominal !== undefined && nominal !== '') ? parseInt(nominal) : 0;
                montant = nbr * nominal;
                $('.montant').val(montant);
            });
            
            $('.nominal').change(function(){
                var nominal = $(this).val();
                var souscrit = $('.souscrit').val();
                nominal = (nominal !== undefined && nominal !== '') ? parseInt(nominal) : 0;
                nbr = (souscrit !== undefined && souscrit !== '') ? parseInt(souscrit) : 0;
                montant = nbr * nominal;
                $('.montant').val(montant);
            });
        });
    }
}