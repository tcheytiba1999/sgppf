export default class Plugins {
    static init (){
        const $ = require('jquery');
        var date = new Date;
        var charg_bef = date.getTime();

        $(function () {
            $('[data-tooltip]').tooltip({
                placement: 'top',
                boundary: 'window',
                delay: { "show": 0, "hide": 0 }
            })
            
            date = new Date;
			var charg_aft = date.getTime();
			var time = (charg_aft-charg_bef)/1000;
            $(".loader").fadeOut(time);
        })
    }
}