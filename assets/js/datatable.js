import 'datatables.net';
import 'datatables.net-bs4';

export default class Datatable {
    static init (){
        const $ = require('jquery');
        $(document).ready( function () {
            $('.dataTb').DataTable({
                responsive: true,
                scrollCollapse: true,
                iDisplayLength: 5,
                pagingType: 'full_numbers',
                language: {
                    sProcessing: "Traitement en cours...",
                    sSearch: "",
                    sLengthMenu: "_MENU_",
                    sInfo: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    sInfoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    sInfoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    sInfoPostFix: "",
                    sLoadingRecords: "Veuillez patientez...",
                    sZeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    sEmptyTable: "Aucune donn&eacute;e disponible dans le tableau",
                    oPaginate: {
                        sFirst: "Premier",
                        sPrevious: "Pr&eacute;c&eacute;dent",
                        sNext: "Suivant",
                        sLast: "Dernier"
                    },
                    oAria: {
                        sSortAscending: ": activer pour trier la colonne par ordre croissant",
                        sSortDescending: ": activer pour trier la colonne par ordre d&eacute;croissant"
                    }
                },
                //Calcul du total des montants pour l'ensemble des tablaux
                footerCallback: function(row, data, start, end, display) {
                        var api = this.api(), data;
                        //recupere la position de la column à calculer
                        var column = $('tfoot th').attr('colspan');
                        //remove the formarring to get integer data for summation
                        var intVal = function (i) {
                            return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ? i : 0;
                        }

                        //total over all pages
                        var total = api.column(column).data().reduce(function(a, b){
                            $(a).eq(column).addClass('text-success');
                            return intVal(a) + intVal(b);
                        }, 0);

                        //Total over this  page
                        var pageTotal = api.column(column, {page: 'current'}).data().reduce(function(a, b){
                            return intVal(a) + intVal(b);
                        }, 0);

                        //update footer
                        var pageTotalF = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'XOF' }).format(pageTotal);
                        var totalF = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'XOF' }).format(total);
                        $(api.column(column).footer()).html(
                            pageTotalF + ' / ( '+totalF+ ' TOTAL)'
                        );
                }
            });
        });
    }
}